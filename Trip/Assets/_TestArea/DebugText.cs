﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class DebugText : MonoBehaviour {

	/// <summary>
	/// The text.
	/// </summary>
	private Text text;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
		//Initialise the text
		text = this.GetComponent<Text>();
	}

	/// <summary>
	/// Sets the text.
	/// </summary>
	/// <param name="message">Message.</param>
	public void SetText (string message)
	{
		text.text = message;
	}
}
