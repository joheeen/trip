﻿using System;
using NUnit.Framework;
using UnityEngine;

[TestFixture]
[Category("Calculation Tests")]
public class DirectionCalculatorTest {

	[Test]
	public void TestDirectionCalculationBetweenPoints () 
	{
		Vector2 A = new Vector2(0, 0);
		Vector2 B = new Vector2(1, 0);
		Vector2 C = new Vector2(1, -1);
		Vector2 D = new Vector2(0, -1);

		CalculateDirectionsBetweenPoints(A, B, Direction.right);
		CalculateDirectionsBetweenPoints(B, C, Direction.down);
		CalculateDirectionsBetweenPoints(C, D, Direction.left);
		CalculateDirectionsBetweenPoints(D, A, Direction.up);
	}


	public void CalculateDirectionsBetweenPoints(Vector2 A, Vector2 B, Direction expectedOutcome)
	{
		Direction AB = DirectionCalculator.GetDirectionBetweenPoints(A, B);

		Assert.AreEqual(expectedOutcome, AB, "Positions: " + A.ToString() + ":" + B.ToString() + " Expected outcome: " + expectedOutcome.ToString() + " Actual outcome " + AB.ToString());
	}

}

