﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChapterScrollController : MonoBehaviour
{
	/// <summary>
	/// The chapter view.
	/// </summary>
	public ChapterView chapterView;

	/// <summary>
	/// The path to scroll by
	/// </summary>
	public ChapterPath path;

	/// <summary>
	/// The animation controller.
	/// </summary>
	public AnimationController animationController;

	/// <summary>
	/// The main camera reference.
	/// </summary>
	public Camera mainCamera;

	/// <summary>
	/// The scrolling speed.
	/// </summary>
	public float scrollSpeedFactor = 0.008f;

	/// <summary>
	/// The scroll speed.
	/// </summary>
	private float scrollSpeed;

	/// <summary>
	/// The drag speed.
	/// </summary>
	public float dragSpeed = 0.1f;

	/// <summary>
	/// The last scroll direction.
	/// </summary>
	private ScrollDirection lastScrollDirection = ScrollDirection.none;

	/// <summary>
	/// The scroll direction offset.
	/// </summary>
	[Range(0, 50)]
	public int scrollDirectionOffset = 20;

	/// <summary>
	/// Gets the t.
	/// </summary>
	/// <value>The t.</value>
	[HideInInspector]
	public float t
	{
		get{ return _t; }
	}

	/// <summary>
	/// The t.
	/// </summary>
	private float _t;

	/// <summary>
	/// Is it allowed to show the finish.
	/// </summary>
	private bool showFinish;

	/// <summary>
	/// The t to result panel.
	/// </summary>
	[Range(0f, 1f)]
	public float tOfResultPanel = 0.9f;

	/// <summary>
	/// The result panel. Used to set the position on the path
	/// </summary>
	public GameObject panelToStopUntil;

	/// <summary>
	/// The direction to Scroll to. Forward or backward.
	/// </summary>
	public enum ScrollDirection
	{
		forward,
		backward,
		none
	}
		
	/// <summary>
	/// The current path segment.
	/// </summary>
	private PathSegment currentPathSegment, previousPathSegment, nextPathSegment;
		
	/// <summary>
	/// The touch ended.
	/// </summary>
	private bool touchEnded = false;

	/// <summary>
	/// Scroll delegate. Used for all scroll related events
	/// </summary>
	public delegate void ScrollDelegate(float t);

	/// <summary>
	/// Occurs when on scroll.
	/// </summary>
	public event ScrollDelegate OnScroll;

	/// <summary>
	/// The fade image.
	/// </summary>
	public Fade fade;

	/// <summary>
	/// The t for fade to start.
	/// </summary>
	[Range(0f, 1f)]
	public float tForFadeToStart;

	/// <summary>
	/// Awake this instance
	/// </summary>
	void Awake () 
	{
		chapterView = chapterView.CheckIfNull();

		//Set the event listeners
		chapterView.touchBegan += TouchOnViewBegan;
		chapterView.touchMoved += TouchOnViewMoved;
		chapterView.touchEnded += TouchOnViewEnded;

		if (animationController != null)
		{
			//Link the ChapterScrollEvent to the Animation controller events 
			OnScroll += animationController.SetScrollTForAnimation;
		}

		//Default checks
		path = path.CheckIfNull();

		//Default checks
		mainCamera = mainCamera.CheckIfNull();

		//default value
		touchEnded = false;

		//Set the showfinish boolean
		if (panelToStopUntil == null)
		{
			showFinish = true;
		} else
		{
			showFinish = false;
		}

	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
	{
		//Set the cameraPosition to the first point in the path
		mainCamera.transform.position = new Vector3(path.points[0].transform.position.x, path.points[0].transform.position.y, mainCamera.transform.position.z);

		//Set the path segments
		currentPathSegment = path.segments[0];
		nextPathSegment = path.segments[1];

		if (GameControllerSingleton.sharedInstance != null)
		{
			//Finish event
			GameControllerSingleton.sharedInstance.allAnswersAreCorrect += AllowScrollToFinish;
		}
	}


	/// <summary>
	/// Activates the gesture system.
	/// </summary>
	/// <returns>The gesture system.</returns>
	void Update()
	{
		//Apply the drag when the touch has stopped
		if (touchEnded)
		{
			scrollSpeed -= dragSpeed * Time.deltaTime;

			if (scrollSpeed < 0)
			{
				scrollSpeed = 0;
				touchEnded = false;
			} 
			else 
			{
				//Do Scroll
				ScrollOverPath(lastScrollDirection);
			}
		}

		if (fade != null)
		{
			//Check if the fade needs to start
			if (_t > tForFadeToStart)
			{
				float alpha = (_t - tForFadeToStart) / (1- tForFadeToStart);

				fade.SetAlpha(alpha);
			}
		}
	}

	/// <summary>
	/// what to do when the single touch has begun.
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchOnViewBegan(Touch touch) 
	{
		touchEnded = false;
	}

	/// <summary>
	/// What to do when the single touch on the view moved.
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchOnViewMoved(Touch touch) 
	{
		//Store the swipedirection
		Direction swipeDirection = DirectionCalculator.GetDirectionBetweenPoints(new Vector2(0, 0), touch.deltaPosition);

		//Screen size independent swipe speed
		float swipeSpeed = new Vector2(touch.deltaPosition.x / Screen.width, touch.deltaPosition.y / Screen.height).magnitude / touch.deltaTime;

		//Bugfix where swipespeed could reach infinity
		if (swipeSpeed == Mathf.Infinity)
		{
			return;
		}

		//Calculate the speed of the scroll
		scrollSpeed = swipeSpeed * scrollSpeedFactor;

		//Scroll forward, none or backward over the path
		lastScrollDirection = DetermineDirectionToScrollTo(swipeDirection, currentPathSegment.directionBetweenPoints);

		Scroll(lastScrollDirection, swipeDirection);
	}

	/// <summary>
	/// What to do when the touch in the view has ended
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchOnViewEnded(Touch touch) 
	{
		touchEnded = true;
	}

	/// <summary>
	/// Scroll the specified lastScrollDirection and swipeDirection.
	/// </summary>
	/// <param name="lastScrollDirection">Last scroll direction.</param>
	/// <param name="swipeDirection">Swipe direction.</param>
	void Scroll(ScrollDirection lastScrollDirection, Direction swipeDirection)
	{
		ScrollOverPath(lastScrollDirection);

		//Check if the camera is within the offset of the second point. If true, than the scroll direction for the nextSegment is also active
		if ( Vector2.Distance(mainCamera.transform.position, currentPathSegment.secondPoint.transform.position) < scrollDirectionOffset )
		{
			//Not the last segment
			if (nextPathSegment != null)
			{
				//Scroll forward, none or backward over the path
				ScrollOverPath(DetermineDirectionToScrollTo(swipeDirection, nextPathSegment.directionBetweenPoints));
			}
		}
		//Check if the camera is within the offset of the first point. If true, than the scroll direction for the previousSegment is also active
		if ( Vector2.Distance(mainCamera.transform.position, currentPathSegment.firstPoint.transform.position) < scrollDirectionOffset )
		{
			//Not the first segment
			if (previousPathSegment != null)
			{
				//Scroll forward, none or backward over the path
				ScrollOverPath(DetermineDirectionToScrollTo(swipeDirection, previousPathSegment.directionBetweenPoints));
			}
		}
	}

	/// <summary>
	/// Scrolls the over path.
	/// </summary>
	/// <param name="scrollSpeed">Scroll speed.</param>
	/// <param name="indexA">Index a.</param>
	/// <param name="indexB">Index b.</param>
	void ScrollOverPath(ScrollDirection direction)
	{
		//Scrollspeed
		switch(direction)
		{
		case ScrollDirection.forward:
			_t += scrollSpeed;
			if (!showFinish)
			{
				if (_t > tOfResultPanel)
				{
					_t = tOfResultPanel;
				}
			}
			else
			{
				if (_t > 1)
				{
					_t = 1;
				}
			}
			break;
		case ScrollDirection.backward:
			_t -= scrollSpeed;
			if (_t < 0)
			{
				_t = 0;
			}
			break;
		default:
			if (_t > 1)
			{
				_t = 1;
			}
			if (_t < 0)
			{
				_t = 0;
			}
			break;
		}
			
		//Get position on path to scroll to
		Vector2 positionToScrollTo = path.GetPositionOnPath(_t, ref currentPathSegment, ref nextPathSegment, ref previousPathSegment);

		//Set the position of the maincamera
		mainCamera.transform.position = new Vector3(positionToScrollTo.x, positionToScrollTo.y, mainCamera.transform.position.z);

		if (OnScroll != null)
		{
			//Invoke all the OnScroll event
			OnScroll(_t);
		}
	}

	/// <summary>
	/// Determines the direction to scroll to. Checks the swipedirections with the pathDirection and returns forward or backward.
	/// </summary>
	/// <returns>The direction to scroll to.</returns>
	/// <param name="touchDelta">Touch delta.</param>
	ScrollDirection DetermineDirectionToScrollTo(Direction swipeDirection, Direction pathDirection)
	{
		switch (pathDirection) 
		{
		case Direction.down:

			if (swipeDirection == Direction.up)
			{
				return ScrollDirection.forward;
			} 
			else if (swipeDirection == Direction.down)
			{
				return ScrollDirection.backward;
			}

			break;
		case Direction.up:

			if (swipeDirection == Direction.down)
			{
				return ScrollDirection.forward;
			} 
			else if (swipeDirection == Direction.up)
			{
				return ScrollDirection.backward;
			}

			break;
		case Direction.left:

			if (swipeDirection == Direction.right)
			{
				return ScrollDirection.forward;
			} 
			else if (swipeDirection == Direction.left)
			{
				return ScrollDirection.backward;
			}

			break;
		case Direction.right:

			if (swipeDirection == Direction.left)
			{
				return ScrollDirection.forward;
			} 
			else if (swipeDirection == Direction.right)
			{
				return ScrollDirection.backward;
			}

			break;
		}
		//Default
		return ScrollDirection.none;
	}

	/// <summary>
	/// Allows the scroll to finish.
	/// </summary>
	void AllowScrollToFinish()
	{
		Debug.Log("All correct answers event invoked");

		//Set the show finish boolean to true
		showFinish = true;
	}

	/// <summary>
	/// Overrides the show finish to true.
	/// </summary>
	public void OverrideShowFinishToTrue()
	{
		showFinish = true;
	}
		
	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy()
	{
		//Remove event listeners
		chapterView.touchBegan -= TouchOnViewBegan;
		chapterView.touchMoved -= TouchOnViewMoved;
		chapterView.touchEnded -= TouchOnViewEnded;

		if (GameControllerSingleton.sharedInstance != null)
		{
			//Finish event
			GameControllerSingleton.sharedInstance.allAnswersAreCorrect -= AllowScrollToFinish;
		}

		if (animationController != null)
		{
			//Link the ChapterScrollEvent to the Animation controller events 
			OnScroll -= animationController.SetScrollTForAnimation;
		}
	}
}