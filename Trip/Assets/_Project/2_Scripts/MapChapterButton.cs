﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Map chapter button.
/// </summary>
public class MapChapterButton : Button 
{
	/// <summary>
	/// Map controller button delegate.
	/// </summary>
	public delegate void MapControllerButtonDelegate(MapChapterButton sender);

	/// <summary>
	/// The button pressed.
	/// </summary>
	public MapControllerButtonDelegate buttonPressed;

	/// <summary>
	/// The name of scene to load.
	/// </summary>
	public string nameOfSceneToLoad;

	/// <summary>
	/// The selectable sprite.
	/// </summary>
	public Sprite selectableSprite;

	/// <summary>
	/// The unselectable sprite.
	/// </summary>
	public Sprite unselectableSprite;

	public GameObject line;

	/// <summary>
	/// Raises the pointer click event.
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public override void OnPointerClick (UnityEngine.EventSystems.PointerEventData eventData)
	{
		base.OnPointerClick (eventData);

		//Invoke event
		buttonPressed(this);
	}

	/// <summary>
	/// Sets the button to selectable. Else it is not interactible.
	/// </summary>
	public void SetSelectable()
	{
		image.sprite = selectableSprite;

		interactable = true;

		image.raycastTarget = true;

		if (line != null)
		line.gameObject.SetActive(true);
	}
}
