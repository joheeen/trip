﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Fade : MonoBehaviour {

	/// <summary>
	/// The fade image.
	/// </summary>
	public Image fadeImage;

	/// <summary>
	/// The t.
	/// </summary>
	private float t;

	/// <summary>
	/// The fade in.
	/// </summary>
	private bool fadeIn;

	/// <summary>
	/// The fade out.
	/// </summary>
	private bool fadeOut;

	void Awake()
	{
		//Get image if null
		if (fadeImage == null)
		{
			fadeImage = this.GetComponent<Image>();
		}

		//Disable raycast target
		fadeImage.raycastTarget = false;
	}

	void Update()
	{

		//Fade In
		if (fadeIn)
		{
			if (t > 0)
			{
				//Set t to 0
				t -= Time.deltaTime;

				//Play the fade in animation
				fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, t);
			} 
			else if (fadeImage.gameObject.activeSelf)
			{
				fadeImage.gameObject.SetActive(false);
				fadeIn = false;
			}
		}

		//Fade out
		if (fadeOut)
		{
			if (t < 1)
			{
				//Set t to 0
				t += Time.deltaTime;

				//Play the fade in animation
				fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, t);
			} 
			else if (fadeImage.gameObject.activeSelf)
			{
				fadeImage.gameObject.SetActive(false);
				fadeOut = false;
			}
		}
	}

	/// <summary>
	/// Activates fade in.
	/// </summary>
	public void FadeIn()
	{
		t = 1;
		fadeIn = true;
		fadeImage.gameObject.SetActive(true);
	}

	/// <summary>
	/// Activates fade out.
	/// </summary>
	public void FadeOut()
	{
		t = 0;
		fadeOut = true;
		fadeImage.gameObject.SetActive(true);
	}

	/// <summary>
	/// Sets the t.
	/// </summary>
	/// <param name="t">T.</param>
	public void SetAlpha(float alpha)
	{
		//set active
		if (!fadeImage.gameObject.activeSelf)
		{
			fadeImage.gameObject.SetActive(true);
		}

		//HACK: Next scene loads earlier now. User experience
		alpha -= 0.1f;

		//set alpha
		fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, alpha);
	}
}
