﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimationController : MonoBehaviour {

	/// <summary>
	/// The segment animation prefab.
	/// </summary>
	public GameObject segmentAnimationGroupPrefab;

	/// <summary>
	/// The segment animations.
	/// </summary>
	[SerializeField]
	public List<SegmentAnimationGroup> segmentAnimationGroups;

	/// <summary>
	/// The character animation.
	/// </summary>
	[SerializeField]
	public MainCharacterAnimation characterAnimation;


	#if UNITY_EDITOR

	/// <summary>
	/// Adds a segment animation to the list.
	/// </summary>
	public void AddSegmentAnimationGroup()
	{
		//check
		if (segmentAnimationGroups == null)
			segmentAnimationGroups = new List<SegmentAnimationGroup>();

		//Store the animation ref
		GameObject segmentAnimationGroupGameObject = GameObject.Instantiate(segmentAnimationGroupPrefab);

		//Add transform as child of this
		segmentAnimationGroupGameObject.transform.parent = this.transform;

		//Set the name of the animation object
		segmentAnimationGroupGameObject.name = "segmentAnimationGroup_" + segmentAnimationGroups.Count.ToString();

		//Store the point script
		SegmentAnimationGroup segmentAnimationGroup = segmentAnimationGroupGameObject.GetComponent<SegmentAnimationGroup>();

		//Add to pointslist
		segmentAnimationGroups.Add(segmentAnimationGroup);
	}

	/// <summary>
	/// Deletes the animation.
	/// </summary>
	/// <param name="index">Index.</param>
	public void DeleteAnimationGroup(int index)
	{
		if (segmentAnimationGroups.Count == 0 || segmentAnimationGroups == null)
		{
			Debug.Log("list already empty, or no list");
			return;
		}

		//Destroy object
		DestroyImmediate(segmentAnimationGroups[index].gameObject);

		//Delete from list
		segmentAnimationGroups.RemoveAt(index);
	}

	#endif


	/// <summary>
	/// Sets the scroll T for animation.
	/// </summary>
	/// <param name="t">T.</param>
	public void SetScrollTForAnimation(float t)
	{
		//Check if a segment animation needs to start
		foreach(SegmentAnimationGroup segmentAnimationGroup in segmentAnimationGroups)
		{
			if (segmentAnimationGroup != null)
			segmentAnimationGroup.SetAnimationsT(t);
		}

		//Set the mainCharacterAnimation
		characterAnimation.SetAnimationT(t);
	}
}
