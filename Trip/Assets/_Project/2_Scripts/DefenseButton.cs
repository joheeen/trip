﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SpriteRenderer))]
public class DefenseButton : MonoBehaviour, ISelectable {

	/// <summary>
	/// The sprite renderer.
	/// </summary>
	private SpriteRenderer spriteRenderer;

	/// <summary>
	/// The selected sprite.
	/// </summary>
	public Sprite selectedSprite;

	/// <summary>
	/// The deselected sprite.
	/// </summary>
	public Sprite deselectedSprite;

	/// <summary>
	/// The defence.
	/// </summary>
	public Defense defence;

	/// <summary>
	/// The selectable active. Deactive if the current observation is correct. No need for change
	/// </summary>
	public bool activeSelectable = true;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
		//Initialise the sprite renderer
		spriteRenderer = this.GetComponent<SpriteRenderer>();

		//Add the box collider at awake. This way it matches the size perfectly
		BoxCollider collider = this.gameObject.AddComponent<BoxCollider>();

		//Set bool default
		activeSelectable = true;
	}

	/// <summary>
	/// Raises the selected event.
	/// </summary>
	public void OnSelect()
	{
		if (!activeSelectable)
			return;
		
		//Set the sprite to selected
		spriteRenderer.sprite = selectedSprite;

		//Invoke event
		GameControllerSingleton.sharedInstance.defenseButtonSelected(this);
	}

	/// <summary>
	/// Raises the deselected event.
	/// </summary>
	public void OnDeselect()
	{
		if (!activeSelectable)
			return;
		
		//Nothing here. Is raised in the chapterController

		//Invoke event
		GameControllerSingleton.sharedInstance.defenseButtonDeselected(this);
	}
		
	/// <summary>
	/// Set the deselected state manual.
	/// </summary>
	public void SetDeselected()
	{
		//Set the sprite to deselected
		spriteRenderer.sprite = deselectedSprite;
	}

	/// <summary>
	/// Set the selected state manual.
	/// </summary>
	public void SetSelected()
	{
		//Set the sprite to selected
		spriteRenderer.sprite = selectedSprite;
	}
}
