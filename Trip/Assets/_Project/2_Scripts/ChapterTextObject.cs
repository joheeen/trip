﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Text object.
/// </summary>
[RequireComponent(typeof(TextMesh))]
public class ChapterTextObject : MonoBehaviour, ISelectable {

	/// <summary>
	/// The selected boollean.
	/// </summary>
	private bool selected = false;

	/// <summary>
	/// The text mesh.
	/// </summary>
	private TextMesh textMesh;

	/// <summary>
	/// The color of the idle text.
	/// </summary>
	public Color normalColor = Color.black;

	/// <summary>
	/// The color of the selected text.
	/// </summary>
	public Color selectedColor = new Color(49, 49, 49); //Gray-ish color

	/// <summary>
	/// The height of the collider scale padding.
	/// </summary>
	public float ColliderScalePaddingHeight = 2; 

	/// <summary>
	/// The width of the collider scale padding.
	/// </summary>
	public float ColliderScalePaddingWidth = 2;

	/// <summary>
	/// The choice made in the minimenu.
	/// </summary>
	MiniMenuChoice choice = MiniMenuChoice.None;//

	/// <summary>
	/// The default position.
	/// </summary>
	private Vector3 defaultPosition;

	/// <summary>
	/// The selected position.
	/// </summary>
	private Vector3 selectedPosition;

	/// <summary>
	/// The linked to observation.
	/// </summary>
	public ChapterObservation linkedToObservation;

	/// <summary>
	/// The stamp sprite.
	/// </summary>
	public SpriteRenderer stampSpriteRenderer;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
		//Set default settings
		textMesh = this.GetComponent<TextMesh>();
		textMesh.anchor = TextAnchor.MiddleCenter;

		//Add the box collider at awake. This way it matches the size perfectly
		BoxCollider collider = this.gameObject.AddComponent<BoxCollider>();

		//Apply padding to size
		collider.size = new Vector3(collider.size.x + ColliderScalePaddingWidth, collider.size.y + ColliderScalePaddingHeight, collider.size.z);

		//Set default color
		textMesh.color = normalColor;

		//Set positions
		defaultPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
		selectedPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - 10);
	}

	/// <summary>
	/// Raises the select event.
	/// </summary>
	public void OnSelect() 
	{
		if (!selected)
		{
			selected = true;
//			textMesh.color = selectedColor;
			transform.position = selectedPosition;
			gameObject.layer = LayerMask.NameToLayer("Selected");
		}

		//Invoke event
		GameControllerSingleton.sharedInstance.textObjectSelected(this);
	}

	/// <summary>
	/// Raises the deselect event.
	/// </summary>
	public void OnDeselect() 
	{
		if (selected)
		{
			selected = false;
//			textMesh.color = normalColor;
			transform.position = defaultPosition;
			gameObject.layer = LayerMask.NameToLayer("Default");
		}

		//Invoke event
		GameControllerSingleton.sharedInstance.textObjectDeselected(this);
	}

	/// <summary>
	/// Gets the name.
	/// </summary>
	public string GetText()
	{
		return textMesh.text;
	}

	/// <summary>
	/// Gets the choice made in the minimenu.
	/// </summary>
	/// <returns>The choice.</returns>
	public MiniMenuChoice GetChoice()
	{
		//return the choice, null if it is not set
		return choice;
	}

	/// <summary>
	/// Sets the choice from the minimenu.
	/// </summary>
	/// <param name="Choice">Choice.</param>
	public void SetChoice(MiniMenuChoice Choice)
	{
		//Set the choice
		choice = Choice;

		//Deactivate the stamp if the choice is none
		if (choice == MiniMenuChoice.None)
		{
			DeactivateStamp();
		}
	}

	/// <summary>
	/// Sets the stamp.
	/// </summary>
	/// <param name="stamp">Stamp.</param>
	public void SetStamp(Sprite stamp)
	{
		//Set active if false
		if (!stampSpriteRenderer.gameObject.activeSelf)
		stampSpriteRenderer.gameObject.SetActive(true);

		//Set stamp
		stampSpriteRenderer.sprite = stamp;
	}

	/// <summary>
	/// Deactivates the stamp.
	/// </summary>
	public void DeactivateStamp()
	{
		stampSpriteRenderer.gameObject.SetActive(false);
	}
}
