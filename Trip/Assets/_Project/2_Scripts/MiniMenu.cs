﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Mini menu abstract class.
/// </summary>
public abstract class MiniMenu : MonoBehaviour {

	/// <summary>
	/// Show this menu.
	/// </summary>
	public abstract void Show(ISelectable selectedObject);

	/// <summary>
	/// Hide this menu.
	/// </summary>
	public abstract void Hide();

}
