﻿using UnityEngine;
using System.Collections;
using UnityEditor.UI;
using UnityEditor;

[CustomEditor(typeof(MiniMenuButton))]
public class MiniMenuButtonEditor : ButtonEditor {

	/// <summary>
	/// The mini menu button.
	/// </summary>
	MiniMenuButton miniMenuButton;

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{
		//Setting the miniMenuButton
		miniMenuButton = (MiniMenuButton)target;

		//Enable the base class first. Otherwise there will be a null reference exception in the base.OnInspectorGUI();
		base.OnEnable();
	}

	/// <summary>
	/// Raises the inspector GU event.
	/// </summary>
	public override void OnInspectorGUI()
	{
		//Draw the base default inspector GUI
		base.OnInspectorGUI();

		//Add a field for the selected sprite 
		miniMenuButton.selectedSprite = EditorGUILayout.ObjectField("Selected Sprite", miniMenuButton.selectedSprite, typeof(Sprite), false) as Sprite;

		//Add field for the deselected sprite
		miniMenuButton.deselectedSprite = EditorGUILayout.ObjectField("Deselected Sprite", miniMenuButton.deselectedSprite, typeof(Sprite), false) as Sprite;

		//HACK: Setting something dirty means that the scene can be saved again. Like creating a new gameobject, or changing a transform.
	}
}
