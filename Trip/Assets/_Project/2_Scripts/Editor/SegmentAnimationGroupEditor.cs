﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SegmentAnimationGroup))]
public class SegmentAnimationGroupEditor : Editor {
	
	/// <summary>
	/// The Segment Animation Group.
	/// </summary>
	SegmentAnimationGroup segmentAnimationGroup;

	/// <summary>
	/// The index of the animation to delete with the GUI button.
	/// </summary>
	int animationIndex;

	/// <summary>
	/// The slider value.
	/// </summary>
	private float sliderValue = 0;

	/// <summary>
	/// The previous slider value.
	/// </summary>
	private float prevSliderValue;

	/// <summary>
	/// Raises the enabled event.
	/// </summary>
	void OnEnabled()
	{
		//Store the path ref
		if (segmentAnimationGroup == null)
		{
			segmentAnimationGroup = target as SegmentAnimationGroup;
		}

		//Storing slider value
		prevSliderValue = sliderValue;
	}

	/// <summary>
	/// Raises the scene GU event.
	/// </summary>
	void OnSceneGUI()
	{
		//Don't continue if path is null or points list is empty
		if( segmentAnimationGroup == null)
		{
			segmentAnimationGroup = target as SegmentAnimationGroup;
			return;
		}
	}

	/// <summary>
	/// Raises the inspector GU event.
	/// </summary>
	public override void OnInspectorGUI() 
	{
		//First draw the default inspector
		DrawDefaultInspector();

		//Add the button to the inspector
		if(GUILayout.Button("Add AnimationSegment"))
		{
			segmentAnimationGroup.AddSegmentAnimation();
		}

		//Set the animation index in the inspector
		animationIndex = EditorGUILayout.IntField("Animation index:", animationIndex);

		if(GUILayout.Button("Delete Animation at index"))
		{
			if (animationIndex < segmentAnimationGroup.segmentAnimations.Count)
			{
				segmentAnimationGroup.DeleteAnimation(animationIndex);
			}
			else
			{
				Debug.Log("<color=yellow>Index to high!" + " count: " + segmentAnimationGroup.segmentAnimations.Count.ToString() + "</color>");
			}
		}

		//the current animation label
		GUILayout.Label("preview animations value: " + sliderValue.ToString());

		//the value of the slider
		sliderValue = GUILayout.HorizontalSlider(sliderValue, 0, 1);

		//Value changed
		if (prevSliderValue != sliderValue)
		{
			//Check if there are sprites
			if (segmentAnimationGroup.segmentAnimations != null && segmentAnimationGroup.segmentAnimations.Count != 0)
			{
				//Play the animation according to the slider if there are sprites
				segmentAnimationGroup.PreviewAnimations(sliderValue);
			}
			//Storing the previous slider value
			prevSliderValue = sliderValue;
		}
	}
}
