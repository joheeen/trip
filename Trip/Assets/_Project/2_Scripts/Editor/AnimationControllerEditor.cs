﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(AnimationController))]
public class AnimationControllerEditor : Editor {

	/// <summary>
	/// The animation controller.
	/// </summary>
	AnimationController animationController;

	/// <summary>
	/// The index of the animation to delete with the GUI button.
	/// </summary>
	int animationGroupIndex;

	/// <summary>
	/// Raises the enabled event.
	/// </summary>
	void OnEnabled()
	{
		//Store the path ref
		if (animationController == null)
		{
			animationController = target as AnimationController;
		}
	}

	/// <summary>
	/// Raises the scene GU event.
	/// </summary>
	void OnSceneGUI()
	{
		//Don't continue if path is null or points list is empty
		if( animationController == null)
		{
			animationController = target as AnimationController;
			return;
		}
	}

	/// <summary>
	/// Raises the inspector GU event.
	/// </summary>
	public override void OnInspectorGUI() 
	{
		//First draw the default inspector
		DrawDefaultInspector();

		//Add the button to the inspector
		if(GUILayout.Button("Add AnimationGroup"))
		{
			animationController.AddSegmentAnimationGroup();
		}

		//Set the animation index in the inspector
		animationGroupIndex = EditorGUILayout.IntField("Animation group index:", animationGroupIndex);

		if(GUILayout.Button("Delete AnimationGroup at index"))
		{
			if (animationGroupIndex < animationController.segmentAnimationGroups.Count)
			{
				animationController.DeleteAnimationGroup(animationGroupIndex);
			}
			else
			{
				Debug.Log("<color=yellow>Index to high!" + " count: " + animationController.segmentAnimationGroups.Count.ToString() + "</color>");
			}
		}
	}

}
