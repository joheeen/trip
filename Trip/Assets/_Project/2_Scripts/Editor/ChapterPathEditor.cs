﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Chapter path editor.
/// </summary>
[CustomEditor(typeof(ChapterPath))]
public class ChapterPathEditor : Editor 
{

	/// <summary>
	/// The target property
	/// </summary>
	ChapterPath path;

	/// <summary>
	/// Raises the enabled event.
	/// </summary>
	void OnEnabled()
	{
		//Store the path ref
		if (path == null)
		{
			path = target as ChapterPath;
		}

		path.DrawLine();
	}

	/// <summary>
	/// Raises the scene GU event.
	/// </summary>
	void OnSceneGUI()
	{
		//Don't continue if path is null or points list is empty
		if( path == null || path.points.Count == 0)
		{
			path = target as ChapterPath;
			return;
		}
	}

	/// <summary>
	/// Raises the inspector GU event.
	/// </summary>
	public override void OnInspectorGUI() 
	{
		DrawDefaultInspector();

		//Add the button to the inspector
		if(GUILayout.Button("Add Point"))
		{
			path.CreateAndAddPointToPath();
		}

		if(GUILayout.Button("Delete Last Points"))
		{
			path.DeleteLastPoint();
		}

		if(GUILayout.Button("Delete All Points"))
		{
			path.DeletePoints();
		}
	}
}
