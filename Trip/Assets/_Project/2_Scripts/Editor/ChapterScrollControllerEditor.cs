﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ChapterScrollController))]
public class ChapterScrollControllerEditor : Editor {

	/// <summary>
	/// The chapter scroll controller.
	/// </summary>
	ChapterScrollController chapterScrollController;

	/// <summary>
	/// The preview t.
	/// </summary>
	[Range(0f, 1f)]
	float previewT;

	/// <summary>
	/// The increment decrement value.
	/// </summary>
	float incrementDecrementValue = 0f;

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{
		chapterScrollController = (ChapterScrollController)target;
	}

	/// <summary>
	/// Raises the inspector GU event.
	/// </summary>
	public override void OnInspectorGUI ()
	{
		//Draw the base
		base.OnInspectorGUI ();

		//Create the field
		previewT = EditorGUILayout.Slider("preview t", previewT, 0f, 1f);

		//CreateField for increment and decrement value
		incrementDecrementValue = EditorGUILayout.FloatField("value", incrementDecrementValue);

		//Create button for incrementing and decrementing previewT slower
		if (GUILayout.Button("increment"))
		{
			previewT += incrementDecrementValue;
		}
		//Create button for incrementing and decrementing previewT slower
		if (GUILayout.Button("decrement"))
		{
			previewT -= incrementDecrementValue;
		}

		//Check if GUI changed
		if (GUI.changed)
		{
			//Set the position of the result panel when changed in the GUI
			if (chapterScrollController.panelToStopUntil != null)
			{
				Vector3 positionOnPath = chapterScrollController.path.GetPositionOnPath(chapterScrollController.tOfResultPanel);
				chapterScrollController.panelToStopUntil.transform.position = new Vector3(positionOnPath.x, positionOnPath.y, chapterScrollController.panelToStopUntil.transform.position.z);
			}

			//Preview the camera and animations
			if (chapterScrollController.mainCamera != null)
			{
				Vector2 cameraPosition = chapterScrollController.path.GetPositionOnPath(previewT);
				chapterScrollController.mainCamera.transform.position = new Vector3(cameraPosition.x, cameraPosition.y, chapterScrollController.mainCamera.transform.position.z);
			}

			//Check if a segment animation needs to start
			if (chapterScrollController.animationController != null)
			{
				foreach(SegmentAnimationGroup segmentAnimationGroup in chapterScrollController.animationController.segmentAnimationGroups)
				{
					if (segmentAnimationGroup != null)
						segmentAnimationGroup.PreviewAnimations(previewT);
				}

				//Set the mainCharacterAnimation
				chapterScrollController.animationController.characterAnimation.PreviewPosition(previewT);
			}
		}
	}
}
