﻿using UnityEngine;
using System.Collections;
using UnityEditor;

/// <summary>
/// Point custom editor
/// </summary>
[CustomEditor(typeof(Point))]
public class PointEditor : Editor {

	/// <summary>
	/// The point ref
	/// </summary>
	private Point point;

	/// <summary>
	/// The size of the gizmo handle.
	/// </summary>
	public float handleSize = 5f;

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable() 
	{
		point = (Point)target;
//		Tools.hidden = true;
	}

	/// <summary>
	/// Raises the disable event.
	/// </summary>
	void OnDisable() {
//		Tools.hidden = false;
	}

	/// <summary>
	/// Raises the inspector GU event.
	/// </summary>
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector ();

		//Setting the size of the handle
		handleSize = EditorGUILayout.Slider(handleSize, 1, 5);
	}

	/// <summary>
	/// Raises the scene GU event.
	/// </summary>
	void OnSceneGUI() 
	{
		//Start recording changes
		Undo.RecordObject((Object)point.gameObject, "Move");

		// Set the colour of the next handle to be drawn
		Handles.color = Color.magenta;

		//Check for changes in position
		EditorGUI.BeginChangeCheck( );
		Vector3 pos = Handles.FreeMoveHandle ( point.transform.position,  Quaternion.identity, handleSize, new Vector3(.5f,.5f,.5f), Handles.RectangleCap);

		if( EditorGUI.EndChangeCheck( ) )
		{
			//What to do after
			Undo.RecordObject((Object)point.gameObject, "Move");
			point.transform.position = pos;
		}

		//Trigger the event that draws the line
		point.TriggerAfterMoveEvent();
	}
		

}
