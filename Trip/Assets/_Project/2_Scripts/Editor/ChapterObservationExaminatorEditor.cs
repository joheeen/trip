﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ChapterObservationExaminator))]
public class ChapterObservationExaminatorEditor : Editor {

	/// <summary>
	/// The chapter observation examinator.
	/// </summary>
	ChapterObservationExaminator chapterObservationExaminator;

	/// <summary>
	/// The index to delete when the button is pressed.
	/// </summary>
	int indexToDelete = 0;

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable () {
		if (chapterObservationExaminator == null)
		chapterObservationExaminator = (ChapterObservationExaminator)target;
	}

	/// <summary>
	/// Raises the inspector GU event.
	/// </summary>
	public override void OnInspectorGUI ()
	{
		//Create field to set the amount of answers to continue
		chapterObservationExaminator.numberOfAnswersToContinue = EditorGUILayout.IntField("Amount of correct answers to continue", chapterObservationExaminator.numberOfAnswersToContinue);

		//Initialise a new answer
		if (GUILayout.Button("Add Answer"))
		{
			if (chapterObservationExaminator.possibleAnswers == null)
				chapterObservationExaminator.possibleAnswers = new System.Collections.Generic.List<ChapterObservationAnswer>();

			chapterObservationExaminator.possibleAnswers.Add(new ChapterObservationAnswer());
		}
			
		//If there are no answers. than nothing needs to happen
		if (chapterObservationExaminator.possibleAnswers == null)
		{
			return;
		}

		//Default value
		int answerIndex = 0;

		//Set all the answer inspector fields
		foreach (ChapterObservationAnswer answer in chapterObservationExaminator.possibleAnswers)
		{
			//Nothing to do with no parts
			if (answer.parts == null)
				return;

			//Visual status in inspector
			string status = "Answer index: " + answerIndex.ToString() + " | Number of parts: " + answer.parts.Count.ToString();

			//Possibility to hide the inspector for this answer
			answer.foldOut = EditorGUILayout.Foldout(answer.foldOut, status);

			//What to show when folded out
			if (answer.foldOut)
			{
				//Posibility to add new part
				if (GUILayout.Button("Add Part"))
				{
					if (answer.parts == null)
						answer.parts = new System.Collections.Generic.List<ChapterObservationAnswer.AnswerPart>();

					answer.parts.Add(new ChapterObservationAnswer.AnswerPart());
				}

				//Show the fields for all the active parts
				for (int i = 0; i < answer.parts.Count; i++)
				{
					answer.parts[i].textObject = (ChapterTextObject)EditorGUILayout.ObjectField("TextObject: " + i.ToString(), answer.parts[i].textObject, typeof(ChapterTextObject), true);
					answer.parts[i].choice = (MiniMenuChoice)EditorGUILayout.EnumPopup("Choice: " + i.ToString(), answer.parts[i].choice);
				}

				//Possibility to reset parts when there are parts
				if (answer.parts.Count > 0)
				{
					if (GUILayout.Button("Reset Parts"))
					{
						answer.parts.Clear();
					}
				}

				//Display the correct Defense
				answer.defense = (Defense)EditorGUILayout.EnumPopup(answer.defense);
			}

			//Update index visible in the inspector
			answerIndex++;
		}

		//Posibility to delete an answer when there are answers
		if (chapterObservationExaminator.possibleAnswers.Count > 0)
		{
			indexToDelete = EditorGUILayout.IntField("delete at index: ", indexToDelete );

			if (GUILayout.Button("Delete answer at index"))
			{
				if (indexToDelete < chapterObservationExaminator.possibleAnswers.Count)
					chapterObservationExaminator.possibleAnswers.RemoveAt(indexToDelete);
			}
		}

		//Set the observationPanel reference
		chapterObservationExaminator.resultPanel = (ResultPanel)EditorGUILayout.ObjectField("result panel", chapterObservationExaminator.resultPanel, typeof(ResultPanel), true);

		//Set the target dirty when the GUI changed so that changes get saved in the editor
		if (GUI.changed)
		{
			EditorUtility.SetDirty(target);
		}
	}
}
