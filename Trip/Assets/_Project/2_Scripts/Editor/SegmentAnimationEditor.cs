﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SegmentAnimation))]
public class SegmentAnimationEditor : Editor {

	/// <summary>
	/// The animation.
	/// </summary>
	SegmentAnimation segmentAnimation;

	/// <summary>
	/// The slider value.
	/// </summary>
	private float sliderValue = 0;

	/// <summary>
	/// The previous slider value.
	/// </summary>
	private float prevSliderValue;

	/// <summary>
	/// The empty sprite.
	/// </summary>
	[SerializeField]
	private Sprite defaultEmptySprite;

	/// <summary>
	/// The first sprite empty.
	/// </summary>
	private bool firstSpriteNeedsToBeEmpty = true;

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable() 
	{
		//Intiliase the target
		segmentAnimation = (SegmentAnimation)target;

		//Set the default empty sprite
		if (defaultEmptySprite == null)
		{
			defaultEmptySprite = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/_Project/6_Sprites/default_empty_sprite.png");
		}

		//Setting the prev slider value
		prevSliderValue = sliderValue;
	}
		
	/// <summary>
	/// Raises the inspector GU event.
	/// </summary>
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector ();

		//A toggle for the first sprite
		firstSpriteNeedsToBeEmpty = EditorGUILayout.Toggle("Empty first sprite", firstSpriteNeedsToBeEmpty);
		CheckFirstSprite();

		//Store the position
		if (GUILayout.Button("Store current position as start position"))
		{
			segmentAnimation.startPosition = segmentAnimation.gameObject.transform.position;
			EditorUtility.SetDirty(target);
		}

		//Store the position
		if (GUILayout.Button("Store current position as middle position"))
		{
			segmentAnimation.middlePosition = segmentAnimation.gameObject.transform.position;
			EditorUtility.SetDirty(target);
		}

		//Store the position
		if (GUILayout.Button("Store current position as end position"))
		{
			segmentAnimation.endPosition = segmentAnimation.gameObject.transform.position;
			EditorUtility.SetDirty(target);
		}

		//the current animation label
		GUILayout.Label("preview animation value: " + sliderValue.ToString());

		//the value of the slider
		sliderValue = GUILayout.HorizontalSlider(sliderValue, 0, 1);

		//Slider changed value changed
		if (prevSliderValue != sliderValue)
		{
			//Check if there are sprites
			if (segmentAnimation.sprites != null && segmentAnimation.sprites.Count != 0)
			{
				//Play the animation according to the slider if there are sprites
				segmentAnimation.PreviewAnimation(sliderValue);
			}
			//storing the previous slider value
			prevSliderValue = sliderValue;
		}
	}

	/// <summary>
	/// Firsts the sprite checkbox clicked.
	/// </summary>
	void CheckFirstSprite()
	{
		//Check if the sprites need an empty first
		if (firstSpriteNeedsToBeEmpty && segmentAnimation.sprites != null && segmentAnimation.sprites.Count != 0)
		{
			//Is the first sprite empty?
			if (segmentAnimation.sprites[0] != defaultEmptySprite)
			{
				segmentAnimation.sprites.Insert(0, defaultEmptySprite);
			}
		} //Needs to be not empty
		else if (!firstSpriteNeedsToBeEmpty && segmentAnimation.sprites != null && segmentAnimation.sprites.Count != 0)
		{
			//Is the first sprite empty?
			if (segmentAnimation.sprites[0] == defaultEmptySprite)
			{
				//Delete the empty sprite
				segmentAnimation.sprites.RemoveAt(0);
			}
		}
	}
}
