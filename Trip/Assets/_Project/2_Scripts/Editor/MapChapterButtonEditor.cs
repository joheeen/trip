﻿using UnityEngine;
using System.Collections;
using UnityEditor;

/// <summary>
/// Map chapter button editor.
/// </summary>
[CustomEditor(typeof(MapChapterButton))]
public class MapChapterButtonEditor : Editor {

	/// <summary>
	/// The map chapter button.
	/// </summary>
	MapChapterButton mapChapterButton;

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{
		mapChapterButton = (MapChapterButton)target;
	}

	public override void OnInspectorGUI ()
	{
		//Draw the base inspector
		DrawDefaultInspector();
	}
}
