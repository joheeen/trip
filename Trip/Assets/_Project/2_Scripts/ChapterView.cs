﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Chapter view.
/// </summary>
public class ChapterView : MonoBehaviour {

	/// <summary>
	/// Single touch delegate.
	/// </summary>
	public delegate void singleTouchDelegate(Touch touch);
	/// <summary>
	/// Occurs when touch began.
	/// </summary>
	public event singleTouchDelegate touchBegan, touchEnded, touchMoved;

	public void Update()
	{
		//One or more touches recognized
		if (Input.touches.Length > 0)
		{
			//Get the prime touch
			Touch touch = Input.GetTouch(0);

			switch (touch.phase)
			{
			case TouchPhase.Began:

				if (touchBegan != null)
				touchBegan(touch);

				break;

			case TouchPhase.Moved:

				if (touchMoved != null)
				touchMoved(touch);

				break;

			case TouchPhase.Ended:

				if (touchEnded != null)
				touchEnded(touch);

				break;
			}
		}
	}

}
