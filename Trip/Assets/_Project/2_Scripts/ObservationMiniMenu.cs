﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine.EventSystems;

[RequireComponent (typeof(RectTransform))]
public class ObservationMiniMenu : MiniMenu {

	/// <summary>
	/// The selected text object.
	/// </summary>
	private ChapterTextObject selectedTextObject;

	/// <summary>
	/// The situation button.
	/// </summary>
	public MiniMenuButton symbolicSituationButton;

	/// <summary>
	/// The reaction button.
	/// </summary>
	public MiniMenuButton reactionButton;

	/// <summary>
	/// The symbol button.
	/// </summary>
	public MiniMenuButton symbolButton;

	/// <summary>
	/// The sensoryPerceptionButton.
	/// </summary>
	public MiniMenuButton sensoryPerceptionButton;

	/// <summary>
	/// The none button.
	/// </summary>
	public MiniMenuButton cancelChoicesButton;

	/// <summary>
	/// The link button.
	/// </summary>
	public MiniMenuButton linkButton;

	/// <summary>
	/// The meaning button.
	/// </summary>
	public MiniMenuButton meaningButton;

	/// <summary>
	/// On text select event.
	/// </summary>
	public delegate void OnTextSelectDelegate(ChapterTextObject textObject);

	/// <summary>
	/// Occurs when text choice made.
	/// </summary>
	public event OnTextSelectDelegate ChoiceMadeEventNotNone;

	/// <summary>
	/// Occurs when text link button pressed.
	/// </summary>
	public event OnTextSelectDelegate LinkButtonPressedEvent;

	/// <summary>
	/// The buttons. Used to make the code more readable
	/// </summary>
	private List<MiniMenuButton> buttons;

	/// <summary>
	/// The symbolic situation stamp.
	/// </summary>
	public Sprite symbolicSituationStamp;

	/// <summary>
	/// The reaction stamp.
	/// </summary>
	public Sprite reactionStamp;

	/// <summary>
	/// The symbol stamp.
	/// </summary>
	public Sprite symbolStamp;

	/// <summary>
	/// The sensory perception stamp.
	/// </summary>
	public Sprite sensoryPerceptionStamp;

	/// <summary>
	/// The meaning stamp.
	/// </summary>
	public Sprite meaningStamp;

	/// <summary>
	/// The rect transform.
	/// </summary>
	private RectTransform rectTransform;

	/// <summary>
	/// The camera.
	/// </summary>
	public Camera camera;

	/// <summary>
	/// Show this menu.
	/// </summary>
	/// <param name="selectedObject">Selected object.</param>
	public override void Show(ISelectable selectedObject)
	{
		//Store the textObject
		selectedTextObject = (ChapterTextObject)selectedObject;

		//Don't show miniMenu if the selectedTextObject belongs to a correct observation
		if (selectedTextObject.linkedToObservation != null)
		{
			//If the observation is correct
			if (selectedTextObject.linkedToObservation.correct)
			{
				return;
			}
		}

		//Else do all this \/

		//Set the position of the minimenu
		rectTransform.transform.position = camera.WorldToScreenPoint(selectedTextObject.gameObject.transform.position);

		//Set the rotation of the minimenu
		rectTransform.transform.rotation = selectedTextObject.gameObject.transform.rotation;

		//check if the linkButton needs to shine
		if (selectedTextObject.linkedToObservation != null)
		{
			//Select or deselect
			if (selectedTextObject.linkedToObservation.GetOptionalTextObjects() != null)
			{
				//More textObjects linked
				if (selectedTextObject.linkedToObservation.GetOptionalTextObjects().Count > 1)
				{
					//Show selected
					linkButton.Select();
				} 
				else
				{
					//show deselected
					linkButton.Deselect();
				}
			}
			else
			{
				//show deselected
				linkButton.Deselect();
			}
		}
		else
		{
			//show deselected
			linkButton.Deselect();
		}

		//If choice is made on Object
		if (selectedTextObject.GetChoice() != MiniMenuChoice.None)
		{
			//Deactivate all other buttons
			DeselectAllButtonsExcept(selectedTextObject.GetChoice());
		}
		//Show all buttons
		else
		{
			//Show all the buttons
			foreach(MiniMenuButton button in buttons)
			{
				//Set all the buttons to default state
				button.Deselect();
			}
		}
			
		//instead of animation
		gameObject.SetActive(true);
	}

	/// <summary>
	/// Hide this menu.
	/// </summary>
	public override void Hide()
	{
		//Instead of animation
		gameObject.SetActive(false);
	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		//Setting the button listener methods
		symbolicSituationButton.onClick.AddListener(() => ChoiceButtonPressed(symbolicSituationButton, MiniMenuChoice.SymbolicSituation));
		reactionButton.onClick.AddListener(() => ChoiceButtonPressed(reactionButton, MiniMenuChoice.Reaction));
		cancelChoicesButton.onClick.AddListener(() => ChoiceButtonPressed(cancelChoicesButton, MiniMenuChoice.None));

		//Add the linkButton eventlistener. Do not add in the buttons list.
		linkButton.onClick.AddListener(() => LinkButtonPressedEvent(selectedTextObject));

		//Initialise buttons
		buttons = new List<MiniMenuButton>();

		//Add all buttons to the list
		buttons.InsertRange(buttons.Count, new List<MiniMenuButton>() { symbolicSituationButton, reactionButton, cancelChoicesButton } );

		//if observation fase 2
		if (GameControllerSingleton.sharedInstance.observationLvl == GameControllerSingleton.ObservationLvl.fase2)
		{
			//add the following buttons
			symbolButton.onClick.AddListener(() => ChoiceButtonPressed(symbolButton, MiniMenuChoice.Symbol));
			sensoryPerceptionButton.onClick.AddListener(() => ChoiceButtonPressed(sensoryPerceptionButton, MiniMenuChoice.SensoryPerception));
			meaningButton.onClick.AddListener(() => ChoiceButtonPressed(meaningButton, MiniMenuChoice.Meaning));

			//Add the rest of the buttons to the list
			buttons.InsertRange(buttons.Count, new List<MiniMenuButton>() { sensoryPerceptionButton, symbolButton, meaningButton } );
		} 
		else
		{
			//Deactivate the unnecesary button
			symbolButton.gameObject.SetActive(false);
			sensoryPerceptionButton.gameObject.SetActive(false);
			meaningButton.gameObject.SetActive(false);
		}

		//Get the rect transform
		rectTransform = this.GetComponent<RectTransform>();

		//Set to default state
		Hide();
	}

	/// <summary>
	/// Choices the button pressed.
	/// </summary>
	/// <param name="Choice">Choice.</param>
	public void ChoiceButtonPressed(MiniMenuButton sender, MiniMenuChoice Choice)
	{
		//Stamp the text object
		SetStampOnTextObject(selectedTextObject, Choice);

		//Return to default when choice is none or the same button is clicked
		if (Choice == MiniMenuChoice.None || Choice == selectedTextObject.GetChoice())
		{
			SetChoiceToDefaultNone();
			return;
		}
			
		//Set the choice
		selectedTextObject.SetChoice(Choice);

		//Invoke the event
		ChoiceMadeEventNotNone(selectedTextObject);

		//Select the button
		DeselectAllButtonsExcept(sender);

		//Show the linkButton
		linkButton.gameObject.SetActive(true);
	}

	/// <summary>
	/// Deselects all buttons except.
	/// </summary>
	/// <param name="button">Button.</param>
	public void DeselectAllButtonsExcept(MiniMenuButton sender)
	{
		//Loop through all the buttons in the miniMenu
		foreach (MiniMenuButton button in buttons)
		{
			//Button is the sender
			if (button == sender)
			{
				button.Select();
			}
			else //All other buttons
			{
				button.Deselect();
			}
		}
	}

	/// <summary>
	/// Deselects all buttons except.
	/// </summary>
	/// <param name="button">Button.</param>
	public void DeselectAllButtonsExcept(MiniMenuChoice choice)
	{
		MiniMenuButton button;

		switch (choice)
		{
		case MiniMenuChoice.None:
			button = cancelChoicesButton;
			break;
		case MiniMenuChoice.Reaction:
			button = reactionButton;
			break;
		case MiniMenuChoice.SensoryPerception:
			button = sensoryPerceptionButton;
			break;
		case MiniMenuChoice.Symbol:
			button = symbolButton;
			break;
		case MiniMenuChoice.SymbolicSituation:
			button = symbolicSituationButton;
			break;
		case MiniMenuChoice.Meaning:
			button = meaningButton;
			break;
		default:
			Debug.Log("bug: Switch hit default");
			button = cancelChoicesButton;
			break;
		}

		//Deselect with button parameter
		DeselectAllButtonsExcept(button);
	}

	/// <summary>
	/// Sets the choice to default.
	/// </summary>
	public void SetChoiceToDefaultNone()
	{
		//Set the choice to none
		selectedTextObject.SetChoice(MiniMenuChoice.None);

		//Loop through all the buttons in the miniMenu and deselect them
		foreach (MiniMenuButton button in buttons)
		{
			button.Deselect();
		}

		//Remove the reference from the text of the observation.
		if (selectedTextObject.linkedToObservation != null)
		{
			selectedTextObject.linkedToObservation.RemoveTextObjectFromObservationAndReference(selectedTextObject);
		}
	}

	/// <summary>
	/// Determines whether a touch has hit a 2d raycastable object.
	/// </summary>
	/// <returns><c>true</c> If a button is touched; otherwise, <c>false</c>.</returns>
	public bool IsTouchOnUI(Touch touch)
	{
		//Create temporary raycast results
		List<RaycastResult> tempRaycastResults = new List<RaycastResult>();

		//Create eventdata
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);

		//Set the position of the eventdata based on the touch position
		eventDataCurrentPosition.position = new Vector2(touch.position.x, touch.position.y);

		//Perform raycast
		EventSystem.current.RaycastAll(eventDataCurrentPosition, tempRaycastResults);

		//If raycast hit a ui object. return the number of hits.
		return tempRaycastResults.Count > 0;
	}

	/// <summary>
	/// Sets the stamp on text object.
	/// </summary>
	/// <param name="TextObject">Text object.</param>
	/// <param name="Choice">Choice.</param>
	void SetStampOnTextObject(ChapterTextObject TextObject, MiniMenuChoice Choice)
	{
		switch (Choice)
		{
		case MiniMenuChoice.None:
			TextObject.DeactivateStamp();
			break;
		case MiniMenuChoice.Reaction:
			TextObject.SetStamp(reactionStamp);
			break;
		case MiniMenuChoice.SensoryPerception:
			TextObject.SetStamp(sensoryPerceptionStamp);
			break;
		case MiniMenuChoice.Symbol:
			TextObject.SetStamp(symbolStamp);
			break;
		case MiniMenuChoice.SymbolicSituation:
			TextObject.SetStamp(symbolicSituationStamp);
			break;
		case MiniMenuChoice.Meaning:
			TextObject.SetStamp(meaningStamp);
			break;
		default:
			Debug.Log("bug: Switch hit default");
			TextObject.DeactivateStamp();
			break;
		}

	}
}
