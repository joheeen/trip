﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class JournalController : MonoBehaviour {

	/// <summary>
	/// The pages.
	/// </summary>
	public Sprite[] pages;

	/// <summary>
	/// The image.
	/// </summary>
	public Image image;

	/// <summary>
	/// The next page button.
	/// </summary>
	public Button nextPageButton;

	/// <summary>
	/// The previous page button.
	/// </summary>
	public Button previousPageButton;

	/// <summary>
	/// The index of the current page.
	/// </summary>
	private int currentPageIndex = 0;

	/// <summary>
	/// The view.
	/// </summary>
	public ChapterView view;

	/// <summary>
	/// The minimum distance for swipe.
	/// </summary>
	private float minimumDistanceForSwipe;

	/// <summary>
	/// The distance.
	/// </summary>
	private float distance;

	/// <summary>
	/// The start position.
	/// </summary>
	Vector2 startPosition;

	/// <summary>
	/// The touchmoved.
	/// </summary>
	bool touchmoved;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Awake () 
	{
		//Setting the button listener methods
		nextPageButton.onClick.AddListener(() => NextPage());
		previousPageButton.onClick.AddListener(() => PreviousPage());

		//The current page index
		currentPageIndex = 0;

		//Show the current page
		ShowPage(currentPageIndex);

		//Add the event listeners
		view.touchBegan += TouchStarted;
		view.touchEnded += TouchEnded;
		view.touchMoved += TouchMoved;

		//Set the minimum distance for a swipe
		minimumDistanceForSwipe = Screen.width / 10;
	}

	/// <summary>
	/// Shows the next page.
	/// </summary>Tocuh
	void NextPage()
	{
		//Else it is activated when the journal is not visible
		if (!gameObject.activeSelf)
			return;
		
		//Increment the current page index
		if (currentPageIndex < pages.Length - 1)
		{
			//Update page index
			currentPageIndex++;

			//Show the page
			ShowPage(currentPageIndex);
		}
	}

	/// <summary>
	/// Shows the previous page.
	/// </summary>
	void PreviousPage()
	{
		//Else it is activated when the journal is not visible
		if (!gameObject.activeSelf)
			return;
		
		//Decrement the current page index
		if (currentPageIndex > 0)
		{
			//Update page index
			currentPageIndex--;

			//Show the page
			ShowPage(currentPageIndex);
		}
	}

	/// <summary>
	/// Shows the page.
	/// </summary>
	void ShowPage(int index)
	{
		//Show the page based in the index
		image.sprite = pages[currentPageIndex];
	}

	/// <summary>
	/// Touchs the started.
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchStarted(Touch touch)
	{
		//The start position
		startPosition = touch.position;
	}

	/// <summary>
	/// Touchs the moved.
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchMoved(Touch touch)
	{
		//Set the touch moved
		touchmoved = true;
	}

	/// <summary>
	/// Touchs the ended.
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchEnded(Touch touch)
	{
		if (touchmoved)
		{
			float distance = Vector2.Distance(startPosition, touch.position);

			if (distance > minimumDistanceForSwipe)
			{
				//Get the direction
				switch(DirectionCalculator.GetDirectionBetweenPoints(startPosition, touch.position))
				{
				case Direction.left:
					NextPage();
					break;
				case Direction.right:
					PreviousPage();
					break;
				default:
					break;
				}
			}

			touchmoved = false;
		}
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy()
	{

	}
}
