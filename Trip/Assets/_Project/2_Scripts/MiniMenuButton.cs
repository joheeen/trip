﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class MiniMenuButton : Button {

	/// <summary>
	/// The image.
	/// </summary>
	Image image;

	/// <summary>
	/// The selected sprite.
	/// </summary>
	public Sprite selectedSprite;

	/// <summary>
	/// The deselected sprite.
	/// </summary>
	public Sprite deselectedSprite;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
		//initialising
		image = this.GetComponent<Image>();
	}

	/// <summary>
	/// Select this instance. Sets the selected color or image
	/// </summary>
	public void Select()
	{
		image.sprite = selectedSprite;
	}

	/// <summary>
	/// Deselect this instance. Sets the default color or image
	/// </summary>
	public void Deselect()
	{
		image.sprite = deselectedSprite;
	}
		
//	public bool IsTouchOnButton(Touch touch)
//	{
//		//Deprecated
//		//HACK: IsPointerOverGameObject not working properly. Used Raycastall instead
////		return EventSystem.current.IsPointerOverGameObject(touch.fingerId);
//	}
}
