﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Chapter view controller.
/// </summary>
public class ChapterController : MonoBehaviour 
{
	/// <summary>
	/// The selectable objects.
	/// </summary>
	private ISelectable selectedObject;

	/// <summary>
	/// The chapter view.
	/// </summary>
	public ChapterView chapterView;

	/// <summary>
	/// The chapter view controller.
	/// </summary>
	public ChapterScrollController chapterScrollController;

	/// <summary>
	/// The observation mini menu.
	/// </summary>
	public ObservationMiniMenu observationMiniMenu;

	/// <summary>
	/// The chapter observation controller.
	/// </summary>
	public ChapterObservationController chapterObservationController;

	/// <summary>
	/// The mini menu hit.
	/// </summary>
	private bool uiHit = false;

	/// <summary>
	/// The background blur.
	/// </summary>
	public Camera backgroundBlurCamera;

	/// <summary>
	/// The selected text renderer camera.
	/// </summary>
	public Camera selectedTextRendererCamera;

	/// <summary>
	/// The fade.
	/// </summary>
	public Fade fade;

	/// <summary>
	/// The index of the lvl.
	/// </summary>
	public int lvlIndex;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake () 
	{
		//Safety checks
		chapterView = chapterView.CheckIfNull();
		chapterScrollController = chapterScrollController.CheckIfNull();
	
		//Link the touch events to the chapterview delegates
		chapterView.touchBegan += TouchOnViewBegan;
		chapterView.touchMoved += TouchOnViewMoved;
		chapterView.touchEnded += TouchOnViewEnded;

		//backgroundblur default
		DeActivateBackgroundBlur();
	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		//Link the text methods with the event in the gameController
		GameControllerSingleton.sharedInstance.textObjectSelected += TextSelected;
		GameControllerSingleton.sharedInstance.textObjectDeselected += TextDeselected;

		//start fade
		fade.FadeIn();
	}

	/// <summary>
	/// what to do when the touch on the view has begun.
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchOnViewBegan(Touch touch) 
	{

		//Check if touch hits a button
		if (observationMiniMenu.IsTouchOnUI(touch))
		{
			uiHit = true;

			return;
		}

		//Initialise ray
		Ray ray = Camera.main.ScreenPointToRay(touch.position);
		RaycastHit hit = new RaycastHit();

		//Cast ray
		if (Physics.Raycast(ray, out hit, 100))
		{
			//Try to store the reference to the hit
			ISelectable selectableObjectHit = hit.transform.GetComponent<ISelectable>();

			//User doesn't click a selectable object 
			if (selectableObjectHit == null) 
			{
				//after the selectable object has been selected.
				if (selectedObject != null)
				{
					//Text is deselected
					selectedObject.OnSelect();

					//Return the selectedObject to null
					selectedObject = null;
				}
			} //user correctly clicks a clickable object
			else 
			{
				//Already a button selected
				if (selectedObject != null)
				{
					//Not the same button twice
					if (selectedObject != selectableObjectHit)
					{
						//Text is deselected
						selectedObject.OnDeselect();

						//Store reference if ISelectable is an component.
						selectedObject = selectableObjectHit;
					}
				} 
				else //User clicks a clickable object and there is nothing selected
				{
					//Store reference if ISelectable is an component.
					selectedObject = selectableObjectHit;
				}
			}
		}
		else //No hit at all
		{
			//Return to default if selected object is set
			if (selectedObject != null)
			{
				//Text is deselected
				selectedObject.OnDeselect();

				//return to null if the hit is empty
				selectedObject = null;
			}
		}
	}

	/// <summary>
	/// what to do when the touch on the view has moved.
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchOnViewMoved(Touch touch) 
	{
		//Return to default when the player swipes and there is a object selected
		if (selectedObject != null)
		{
			//Text is deselected
			selectedObject.OnDeselect();

			//return selectedObject to default
			selectedObject = null;
		}

		//return to default state
		uiHit = false;

		//At the end of the chapter
		if(chapterScrollController.t == 1)
		{
			FinishGame();
		}
	}

	/// <summary>
	/// what to do when the touch on the view has ended.
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchOnViewEnded(Touch touch) 
	{
		//button has not been pressed
		if (!uiHit)
		{
			//Select the object when there is no object and the touch ended
			if (selectedObject != null)
			{
				//The text is sucessfully selected
				selectedObject.OnSelect();
			}
		}

		//return to default state
		uiHit = false;
	}

	/// <summary>
	/// Do functionality when text is selected.
	/// </summary>
	void TextSelected(ISelectable textObject)
	{
		//Show background blur
		ActivateBackgroundBlur();

		//Let the observation controller know that the button is pressed
		chapterObservationController.TextObjectClicked((ChapterTextObject)selectedObject);

		//Show the mini menu
		if (observationMiniMenu != null)
		observationMiniMenu.Show(selectedObject);
	}

	/// <summary>
	/// Do functionality when text is deselected.
	/// </summary>
	void TextDeselected(ISelectable textObject)
	{
		//Deactivate blur
		DeActivateBackgroundBlur();

		//Hide the miniMenu
		if (observationMiniMenu != null)
		observationMiniMenu.Hide();
	}

	/// <summary>
	/// Activates the background blur.
	/// </summary>
	void ActivateBackgroundBlur()
	{
		//check
		if (backgroundBlurCamera != null && selectedTextRendererCamera != null)
		{
			backgroundBlurCamera.gameObject.SetActive(true);
			selectedTextRendererCamera.gameObject.SetActive(true);
		}
	}

	/// <summary>
	/// Des the activate background blur.
	/// </summary>
	void DeActivateBackgroundBlur()
	{
		//check
		if (backgroundBlurCamera != null && selectedTextRendererCamera != null)
		{
			backgroundBlurCamera.gameObject.SetActive(false);
			selectedTextRendererCamera.gameObject.SetActive(false);
		}
	}

	/// <summary>
	/// Finishs the game.
	/// </summary>
	void FinishGame()
	{
		Debug.Log("Finish the Game");

		//Lvl index bool
		bool saveLvlIndex = false;

		if (PlayerPrefs.HasKey("currentLvl"))
		{
			int currentLvl = PlayerPrefs.GetInt("currentLvl");

			//Don't need to overwrite data if the lvl has been completed before
			if (lvlIndex + 1 <= currentLvl)
			{
				saveLvlIndex = false;
			} 
			//First time for this lvl
			else
			{
				saveLvlIndex = true;
			}
		}
		//First time for this lvl
		else
		{
			saveLvlIndex = true;
		}

		//Save the lvl index
		if (saveLvlIndex)
		{
			//Store the playerpref
			PlayerPrefs.SetInt("currentLvl", lvlIndex + 1);

			//Save the playerprefs
			PlayerPrefs.Save();
		}

		//Finish the scene
		SceneManager.LoadScene("Map_Scene");
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy()
	{
		chapterView.touchBegan -= TouchOnViewBegan;
		chapterView.touchMoved -= TouchOnViewMoved;
		chapterView.touchEnded -= TouchOnViewEnded;

		//Link the text methods with the event in the gameController
		GameControllerSingleton.sharedInstance.textObjectSelected -= TextSelected;
		GameControllerSingleton.sharedInstance.textObjectDeselected -= TextDeselected;
	}
}
