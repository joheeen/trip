﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Chapter observation examinator. Examinates all the observations at the end of the scene.
/// </summary>
[RequireComponent(typeof(ChapterObservationController))]
[System.Serializable]
public class ChapterObservationExaminator : MonoBehaviour 
{
	/// <summary>
	/// The possible answers.
	/// </summary>
	[SerializeField]
	public List<ChapterObservationAnswer> possibleAnswers;

	/// <summary>
	/// The observation controller.
	/// </summary>
	private ChapterObservationController observationController;

	/// <summary>
	/// The result panel.
	/// </summary>
	public ResultPanel resultPanel;

	/// <summary>
	/// Examinator delegate.
	/// </summary>
	public delegate void ExaminatorUpdateDelegate(ChapterObservation observation);

	/// <summary>
	/// Examinator finish delegate.
	/// </summary>
	public delegate void ExaminatorFinishDelegate();

	/// <summary>
	/// The correct answer.
	/// </summary>
	public ExaminatorUpdateDelegate correctAnswer;

	/// <summary>
	/// The number of answers to continue.
	/// </summary>
	public int numberOfAnswersToContinue = 1;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		//Set the observation controller reference
		observationController = this.GetComponent<ChapterObservationController>();

		//Add the event listeners
		observationController.observationsUpdate += CheckAnswers;

		if (resultPanel != null)
		{
			observationController.observationsUpdate += resultPanel.UpdateResultPanel;
			correctAnswer += resultPanel.CorrectAnswer;
		}

		//Check number of answers to continue
		if (numberOfAnswersToContinue > possibleAnswers.Count)
		{
			numberOfAnswersToContinue = possibleAnswers.Count;
		}
	}

	/// <summary>
	/// Check if there are correct answers.
	/// </summary>
	public void CheckAnswers(List<ChapterObservation> observations)
	{
		//Safety check
		if (observations == null || observations.Count == 0)
			return;

		bool breakOutLoop = false;

		Debug.Log("Checking answers");

		//Check all observations
		foreach (ChapterObservation observation in observations)
		{
			//Check if observation contains list
			List<ChapterTextObject> textObjects = observation.GetOptionalTextObjects();
			if (textObjects == null)
				return;

			//Check all textObjects in observations
			foreach (ChapterTextObject textObject in textObjects)
			{
				//Check all the possible answers to check if they contain the textobject
				foreach (ChapterObservationAnswer answer in possibleAnswers)
				{
					//The answer contains this textobject
					if (answer.ContainsTextObject(textObject))
					{
						//Correct choices made on textObjects
						if (answer.Correct(textObjects))
						{
							//Correct Defense
							if (answer.defense == observation.defense)
							{
								Debug.Log("<color=green>A Correct Answer!!</color>");
								correctAnswer.Invoke(observation);
								observation.correct = true;
								answer.correctlyAnswered = true;
								breakOutLoop = true;
								break;
							}
							else
							{
								Debug.Log("<color=yellow>Correct textobjects but not Defense!!</color>");
								observation.correct = false;
								break;
							}
						}
						else
						{		
							//Break out because the other answers don't need to be checked.
							Debug.Log("<color=red>No correct textobjects!!</color>");
							observation.correct = false;
							break;
						}
					}
					else
					{
						observation.correct = false;
					}
				}

				//Break out loop because the answer of this observation is already correct. No need to check the other textobjects in this observation
				if (breakOutLoop)
					break;
			}
			//Return default
			breakOutLoop = false;
		}

		//Default value
		int amountOfCorrectAnswers = 0;

		//Check if all the answers are correct
		foreach (ChapterObservationAnswer answer in possibleAnswers)
		{
			//If answer is correct
			if (answer.correctlyAnswered)
			{
				amountOfCorrectAnswers++;
			}
		}

		//All answers are correct so invoke event
		if (amountOfCorrectAnswers >= numberOfAnswersToContinue)
		{
			Debug.Log("<color=green>Enough Answers are Correct!! Answers correct: </color>" + amountOfCorrectAnswers);
			GameControllerSingleton.sharedInstance.allAnswersAreCorrect.Invoke();
		}
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy()
	{
		//Remove the event listeners
		observationController.observationsUpdate -= CheckAnswers;

		if (resultPanel != null)
		{
			observationController.observationsUpdate -= resultPanel.UpdateResultPanel;
			correctAnswer -= resultPanel.CorrectAnswer;
		}
	}
}
