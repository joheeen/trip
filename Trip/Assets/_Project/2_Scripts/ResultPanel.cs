﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Result panel.
/// </summary>
[RequireComponent (typeof(SpriteRenderer))]
public class ResultPanel : MonoBehaviour {

	/// <summary>
	/// The sprite renderer.
	/// </summary>
	SpriteRenderer spriteRenderer;

	//All the text meshes
	public TextMesh reactieTextMesh;
	public TextMesh situationTextMesh;
	public TextMesh sensoryPerceptionTextMesh;
	public TextMesh symbolTextMesh;
	public TextMesh meaningTextMesh;

	/// <summary>
	/// The observations as hold by the observation controller.
	/// </summary>
	List<ChapterObservation> observations = new List<ChapterObservation>();

	/// <summary>
	/// The current displayed observation.
	/// </summary>
	ChapterObservation currentDisplayedObservation;

	//All bookmark sprites
	public Sprite OrangeBookMarkSelected;
	public Sprite OrangeBookMarkUnselected;
	public Sprite OrangeBookMarkCorrect;

	public Sprite RedBookMarkSelected;
	public Sprite RedBookMarkUnselected;
	public Sprite RedBookMarkCorrect;

	public Sprite GreenBookMarkSelected;
	public Sprite GreenBookMarkUnselected;
	public Sprite GreenBookMarkCorrect;

	public Sprite BeigeBookMarkSelected;
	public Sprite BeigeBookMarkUnselected;
	public Sprite BeigeBookMarkCorrect;

	public Sprite LightBlueBookMarkSelected;
	public Sprite LightBlueBookMarkUnselected;
	public Sprite LightBlueBookMarkCorrect;

	public Sprite DarkBlueBookMarkSelected;
	public Sprite DarkBlueBookMarkUnselected;
	public Sprite DarkBlueBookMarkCorrect;

	public Sprite MiddleBlueBookMarkSelected;
	public Sprite MiddleBlueBookMarkUnselected;
	public Sprite MiddleBlueBookMarkCorrect;

	public Sprite TurqoiseBlueBookMarkSelected;
	public Sprite TurqoiseBlueBookMarkUnselected;
	public Sprite TurqoiseBlueBookMarkCorrect;

	/// <summary>
	/// The bookmark buttons.
	/// </summary>
	private List<BookmarkButton> bookmarkButtons = new List<BookmarkButton>();

	/// <summary>
	/// The bookmark prefab.
	/// </summary>
	public GameObject bookmarkPrefab;

	/// <summary>
	/// The defense buttons.
	/// </summary>
	public List<DefenseButton> defenseButtons;

	/// <summary>
	/// The default size of the font.
	/// </summary>
	public int defaultFontSize = 64;

	/// <summary>
	/// Start this instance.
	/// </summary>
	public void Start()
	{
		//Set the spriterenderer
		spriteRenderer = this.GetComponent<SpriteRenderer>();

		//Reset the display to default
		ResetResultDisplay();

		//Add eventlisteners
		GameControllerSingleton.sharedInstance.bookmarkButtonSelected += BookmarkButtonPressed;
		GameControllerSingleton.sharedInstance.bookmarkButtonDeselected += BookmarkButtonUnpressed;

		//Link the button methods with the event in the gameController
		GameControllerSingleton.sharedInstance.defenseButtonSelected += DefenseButtonSelected;
		GameControllerSingleton.sharedInstance.defenseButtonDeselected += DefenseButtonDeselected;
	}

	/// <summary>
	/// Updates the result panel.
	/// </summary>
	/// <param name="observations">Observations.</param>
	public void UpdateResultPanel(List<ChapterObservation> Observations)
	{
		//Safety check
		if (Observations == null || Observations.Count == 0)
			return;

		Debug.Log("Result panel updated");

		//Store the updated observations
		observations = Observations;

		//store the first updated observation
		if (currentDisplayedObservation == null)
			currentDisplayedObservation = observations[0];

		//After linking the currectDisplayObservation can be 0
		if (currentDisplayedObservation.GetOptionalTextObjects() == null)
		{
			currentDisplayedObservation = observations[0];
		}

		//Update the result
		DisplayResult(currentDisplayedObservation);
	}

	/// <summary>
	/// Adds the bookmark.
	/// </summary>
	/// <param name="colorEnum">Color enum.</param>
	public void AddBookmark(ChapterObservation observation)
	{
		//The sprites to give the bookmark
		Sprite selectedSprite, deselectedSprite, correctSprite;

		//Switch through all the groupcolors to set the correct bookmark
		switch(observation.groupColor.colorEnum)
		{
		case GroupColorEnum.Beige:

			selectedSprite = BeigeBookMarkSelected;
			deselectedSprite = BeigeBookMarkUnselected;
			correctSprite = BeigeBookMarkCorrect;

			break;
		case GroupColorEnum.DarkBlue:

			selectedSprite = DarkBlueBookMarkSelected;
			deselectedSprite = DarkBlueBookMarkUnselected;
			correctSprite = DarkBlueBookMarkCorrect;

			break;
		case GroupColorEnum.Green:

			selectedSprite = GreenBookMarkSelected;
			deselectedSprite = GreenBookMarkUnselected;
			correctSprite = GreenBookMarkCorrect;

			break;
		case GroupColorEnum.LightBlue:

			selectedSprite = LightBlueBookMarkSelected;
			deselectedSprite = LightBlueBookMarkUnselected;
			correctSprite = LightBlueBookMarkCorrect;

			break;
		case GroupColorEnum.MiddleBlue:

			selectedSprite = MiddleBlueBookMarkSelected;
			deselectedSprite = MiddleBlueBookMarkUnselected;
			correctSprite = MiddleBlueBookMarkCorrect;

			break;
		case GroupColorEnum.Orange:

			selectedSprite = OrangeBookMarkSelected;
			deselectedSprite = OrangeBookMarkUnselected;
			correctSprite = OrangeBookMarkCorrect;

			break;
		case GroupColorEnum.Red:

			selectedSprite = RedBookMarkSelected;
			deselectedSprite = RedBookMarkUnselected;
			correctSprite = RedBookMarkCorrect;

			break;
		case GroupColorEnum.TurqoiseBlue:

			selectedSprite = TurqoiseBlueBookMarkSelected;
			deselectedSprite = TurqoiseBlueBookMarkUnselected;
			correctSprite = TurqoiseBlueBookMarkCorrect;

			break;
		default:
			Debug.Log("Error: Colorenum Switch hit default");
			selectedSprite = RedBookMarkSelected;
			deselectedSprite = RedBookMarkUnselected;
			correctSprite = RedBookMarkCorrect;

			break;
		}

		//Instantiate the gameobject
		InstantiateBookmark(selectedSprite, deselectedSprite, correctSprite, observation);
	}

	/// <summary>
	/// Removes the bookmark.
	/// </summary>
	/// <param name="colorEnum">Color enum.</param>
	public void RemoveBookmark(ChapterObservation observation)
	{
		//Remove the correct bookmark
		foreach (BookmarkButton bookmarkButton in bookmarkButtons)
		{
			//Remove and destroy this bookmarkbutton
			if (bookmarkButton.observation == observation)
			{
				//Remove the reference from the list
				bookmarkButtons.Remove(bookmarkButton);

				//Destroy gameobject from scene
				GameObject.Destroy( bookmarkButton.gameObject );

				//Check the positions of the button(s)
				SetPositionOfBookmarkButtons();

				break;
			}
		}
	}

	/// <summary>
	/// Shows the result.
	/// </summary>
	/// <param name="observation">Observation.</param>
	public void DisplayResult(ChapterObservation observation)
	{
		//Inactive the defense buttons if the observation is correct
		if (observation.correct)
		{
			foreach (DefenseButton defenseButton in defenseButtons)
			{
				defenseButton.activeSelectable = false;
			}
		}
		else
		{
			//Set all the buttons active
			foreach (DefenseButton defenseButton in defenseButtons)
			{
				defenseButton.activeSelectable = true;
			}
		}

		//Get the list with chapter text objects
		List<ChapterTextObject> textObjects = observation.GetOptionalTextObjects();

		//Null
		if (textObjects == null)
			return;

		//Reset all text to ""
		ResetResultDisplay();

		//Display information
		foreach (ChapterTextObject textObject in textObjects)
		{
			//Get the text object choice
			MiniMenuChoice choice = textObject.GetChoice();

			//Switch through the possible choices and set the text accordingly
			switch (choice)
			{
			case MiniMenuChoice.SymbolicSituation:
				situationTextMesh.text = textObject.GetText();
				situationTextMesh.fontSize = defaultFontSize;
				CheckTextMeshSize(situationTextMesh);
				break;
			case MiniMenuChoice.Reaction:
				reactieTextMesh.text = textObject.GetText();
				reactieTextMesh.fontSize = defaultFontSize;
				CheckTextMeshSize(reactieTextMesh);
				break;
			case MiniMenuChoice.SensoryPerception:
				sensoryPerceptionTextMesh.text = textObject.GetText();
				sensoryPerceptionTextMesh.fontSize = defaultFontSize;
				CheckTextMeshSize(sensoryPerceptionTextMesh);
				break;
			case MiniMenuChoice.Symbol:
				symbolTextMesh.text = textObject.GetText();
				symbolTextMesh.fontSize = defaultFontSize;
				CheckTextMeshSize(symbolTextMesh);
				break;
			case MiniMenuChoice.Meaning:
				meaningTextMesh.text = textObject.GetText();
				meaningTextMesh.fontSize = defaultFontSize;
				CheckTextMeshSize(meaningTextMesh);
				break;
			default:
				break;
			}
		}

		//Set the correct Defense button selected
		foreach (DefenseButton defButton in defenseButtons)
		{
			if (defButton.defence == observation.defense)
			{
				defButton.SetSelected();
			}
			else
			{
				defButton.SetDeselected();
			}
		}
	}

	/// <summary>
	/// Resets the result display.
	/// </summary>
	void ResetResultDisplay()
	{
		//TODO: BUG Reactie textmesh is gone
		//Set the text
		reactieTextMesh.text = "";
		situationTextMesh.text = "";
		sensoryPerceptionTextMesh.text = "";
		symbolTextMesh.text = "";
		meaningTextMesh.text = "";
	}

	/// <summary>
	/// Instantiates the bookmark.
	/// </summary>
	/// <param name="selectedSprite">Selected sprite.</param>
	/// <param name="deselectedSprite">Deselected sprite.</param>
	/// <param name="groupColor">Group color.</param>
	void InstantiateBookmark(Sprite selectedSprite, Sprite deselectedSprite, Sprite correctSprite, ChapterObservation observation)
	{
		//Instantiating the bookmark object
		GameObject bookmark = (GameObject)Instantiate(bookmarkPrefab);

		//Get the reference
		BookmarkButton bookmarkButton = bookmark.GetComponent<BookmarkButton>();

		//Set the groupcolor
		bookmarkButton.observation = observation;

		//Add the bookmark button reference
		bookmarkButtons.Add( bookmarkButton );

		//Set the sprites
		bookmarkButton.SetSprites(selectedSprite, deselectedSprite, correctSprite);

		//Set parent
		bookmark.transform.parent = this.gameObject.transform;

		//Set transform rotation
		bookmark.transform.rotation = new Quaternion(0, 0, 0, 0);

		//Check the positions of the button(s)
		SetPositionOfBookmarkButtons();

		//Set the sprite if it's not the first
		if (bookmarkButtons.Count > 1)
		{
			bookmarkButton.SetDeselected();
		}
		//Set the first button selected
		else if (bookmarkButtons.Count == 1)
		{
			bookmarkButton.OnSelect();
		}
	}

	/// <summary>
	/// Sets the position of bookmark buttons.
	/// </summary>
	void SetPositionOfBookmarkButtons()
	{
		//Scale factor. Bounds needs to be devided by this value
		float scaleFactor = gameObject.transform.lossyScale.x;

		//Nothing to do
		if (bookmarkButtons.Count == 0)
		{
			return;
		}

		//Get the bounds value
		Bounds bounds = spriteRenderer.bounds;

		//Just one bookmark
		if (bookmarkButtons.Count == 1)
		{
			//Calculate the y position for this gameobject
			float yPos = bounds.extents.y;

			//HACK:
			if (bounds.size.x > bounds.size.y)
				yPos = bounds.extents.x;
				
			//Set the position of this gameObject
			bookmarkButtons[0].gameObject.transform.localPosition = new Vector3(0, (yPos / scaleFactor), -1f);
		}
		//More than one
		else
		{
			//Create a index value
			int index = 1;

			//Calculate the x fraction for this gameobject
			float xPosFraction = (bounds.size.x / scaleFactor) / (bookmarkButtons.Count + 1);

			//Calculate the y position for this gameobject
			float yPos = bounds.extents.y / scaleFactor;


			//HACK:
			if (bounds.size.x > bounds.size.y)
			{
				xPosFraction = (bounds.size.y / scaleFactor) / (bookmarkButtons.Count + 1);
				yPos = bounds.extents.x / scaleFactor;
			}

			//Set the positions of the button and the other buttons
			foreach(BookmarkButton bookmarkButton in bookmarkButtons)
			{
				//Calculate the x position for this gameobject
				float xPos = (-bounds.extents.x / scaleFactor) + (xPosFraction * index);

				//HACK
				if (bounds.size.x > bounds.size.y)
				{
					xPos = (-bounds.extents.y / scaleFactor) + (xPosFraction * index);
				}

				//Set the position of this gameObject
				bookmarkButton.gameObject.transform.localPosition = new Vector3(xPos, yPos, -1f);

				//increment the index
				index++;
			}
		}
	}

	/// <summary>
	/// Changes the current displayed observation.
	/// </summary>
	/// <param name="chapterObservation">Chapter observation.</param>
	void ChangeCurrentDisplayedObservation(ChapterObservation chapterObservation)
	{
		//Store the ref
		currentDisplayedObservation = chapterObservation;
			
		//display the result
		DisplayResult(currentDisplayedObservation);
	}

	/// <summary>
	/// Bookmarks the button pressed.
	/// </summary>
	void BookmarkButtonPressed(ISelectable bookmarkObject)
	{
		//DownCast to bookmarkbutton
		BookmarkButton bookmarkButton = bookmarkObject as BookmarkButton;

		//No unnecesary displaying
		if (bookmarkButton.observation != currentDisplayedObservation)
		{
			//Display the correct observation
			DisplayResult(bookmarkButton.observation);
		}

		//Set the sprites of the unselected bookmark
		foreach (BookmarkButton button in bookmarkButtons)
		{
			button.SetDeselected();
		}

		//Store the current active bookmark
		currentDisplayedObservation = bookmarkButton.observation;
	}

	/// <summary>
	/// Bookmarks the button unpressed.
	/// </summary>
	void BookmarkButtonUnpressed(ISelectable bookmarkObject)
	{
		Debug.Log("Bookmark button unpressed");
	}

	/// <summary>
	/// Do code when the defense button has been selected
	/// </summary>
	void DefenseButtonSelected(ISelectable button)
	{
		//Downcast the button
		DefenseButton defenseButton = button as DefenseButton;

		//Set all the others deselected
		foreach (DefenseButton defButton in defenseButtons)
		{
			if (defButton != defenseButton)
				defButton.SetDeselected();
		}

		//No observation yet
		if (currentDisplayedObservation == null)
			return;

		//Set the defense
		currentDisplayedObservation.SetDefense( defenseButton.defence );

		//Update results
		DisplayResult( currentDisplayedObservation );
	}

	/// <summary>
	/// Defenses the button deselected.
	/// </summary>
	void DefenseButtonDeselected(ISelectable button)
	{
		Debug.Log("Defense button deselected");
	}

	/// <summary>
	/// Checks the size of the text mesh.
	/// </summary>
	/// <param name="textMesh">Text mesh.</param>
	void CheckTextMeshSize(TextMesh textMesh)
	{
		//Convert the text to fit the resultpanel
		if (textMesh.text.Length > 60)
		{
			string substring = textMesh.text.Substring(0, 60) + "...";
			textMesh.text = substring;
		}

		//Get the meshrenderer
		MeshRenderer meshRenderer = textMesh.gameObject.GetComponent<MeshRenderer>();

		float xSize = (spriteRenderer.bounds.size.x / gameObject.transform.lossyScale.x);
		float ySize = (spriteRenderer.bounds.size.y / gameObject.transform.lossyScale.y);

		//While the text is bigger on the x axis, make it smaller until smaller than the resultpanel
		while (meshRenderer.bounds.size.x > xSize)
		{
			textMesh.fontSize -= 1;
		}

		//While the text is bigger on the y axis, make it smaller until smaller than the resultpanel
		while (meshRenderer.bounds.size.y > ySize)
		{
			textMesh.fontSize -= 1;
		}			
	}

	/// <summary>
	/// Corrects the answer.
	/// </summary>
	/// <param name="observation">Observation.</param>
	public void CorrectAnswer(Observation observation)
	{
		//Storing the correct answer bookmark
		foreach (BookmarkButton button in bookmarkButtons)
		{
			//The button for the correct observation
			if (button.observation == observation)
			{
				button.SetCorrect();
			}
		}
	}

	/// <summary>
	/// Raises the application quit event.
	/// </summary>
	void OnDestroy()
	{
		//Remove eventlisteners
		GameControllerSingleton.sharedInstance.bookmarkButtonSelected -= BookmarkButtonPressed;
		GameControllerSingleton.sharedInstance.bookmarkButtonDeselected -= BookmarkButtonUnpressed;

		//Link the button methods with the event in the gameController
		GameControllerSingleton.sharedInstance.defenseButtonSelected -= DefenseButtonSelected;
		GameControllerSingleton.sharedInstance.defenseButtonDeselected -= DefenseButtonDeselected;
	}
}
