﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Group color enum.
/// </summary>
public enum GroupColorEnum
{
	Orange,
	Red,
	Green,
	Beige,
	LightBlue,
	DarkBlue,
	MiddleBlue,
	TurqoiseBlue
}


