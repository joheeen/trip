﻿/// <summary>
/// Mini menu choices.
/// </summary>
public enum MiniMenuChoice
{
	None,
	Reaction,
	SymbolicSituation,
	Symbol,
	SensoryPerception,
	Meaning
}
