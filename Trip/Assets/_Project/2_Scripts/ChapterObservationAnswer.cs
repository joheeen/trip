﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Chapter observation answer.
/// </summary>
[System.Serializable]
public class ChapterObservationAnswer {

	/// <summary>
	/// Text and type.
	/// </summary>
	[System.Serializable]
	public class AnswerPart
	{
		public ChapterTextObject textObject = new ChapterTextObject();
		public MiniMenuChoice choice = MiniMenuChoice.None;
	}

	/// <summary>
	/// The defense.
	/// </summary>
	public Defense defense;

	/// <summary>
	/// The fold out.
	/// </summary>
	[HideInInspector]
	public bool foldOut = false;

	/// <summary>
	/// The answers.
	/// </summary>
	public List<AnswerPart> parts = new List<AnswerPart>();

	/// <summary>
	/// The correctly answered.
	/// </summary>
	public bool correctlyAnswered = false;

	/// <summary>
	/// Does this answer contain the text object.
	/// </summary>
	/// <returns><c>true</c>, if text object was containsed, <c>false</c> otherwise.</returns>
	/// <param name="textObject">Text object.</param>
	public bool ContainsTextObject(ChapterTextObject textObject)
	{
		foreach(AnswerPart part in parts)
		{
			if (part.textObject == textObject)
				return true;
		}
		return false;
	}

	/// <summary>
	/// Part that contains text object.
	/// </summary>
	/// <returns>The Optional AnswerPart that contains text object.</returns>
	/// <param name="textObject">Text object.</param>
	AnswerPart PartContainsTextObject(ChapterTextObject textObject)
	{
		//Return part is it is correct
		foreach(AnswerPart part in parts)
		{
			if (part.textObject == textObject)
				return part;
		}
		return null;
	}

	/// <summary>
	/// Is the answer correct with the specified textObjects.
	/// </summary>
	/// <param name="textObjects">Text objects.</param>
	public bool Correct(List<ChapterTextObject> textObjects)
	{
		//First check if not to many objects are linked
		if (textObjects.Count != parts.Count)
		{
			Debug.Log("Not the correct amount of textobjects");
			return false;
		}

		//Loop through all the textobjects
		foreach (ChapterTextObject textObject in textObjects)
		{
			AnswerPart optionalAnswerPart = PartContainsTextObject(textObject);

			//optionalAnswerPart contains the text object 
			if (optionalAnswerPart != null)
			{
				//the choice is the same
				if (optionalAnswerPart.choice == textObject.GetChoice())
				{
					//correct. If all are correct, eventually true will be returned
				}
				else
				{
					//TextObject doesn't have the right choice 
					return false;
				}
			} 
			else
			{
				//No AnswerPart has the same textObject
				return false;
			}
		}

		return true;
	}
}
