﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Extension methods are declared here.
/// </summary>
public static class ExtensionMethods
{
	/// <summary>
	/// Checks if the monobehaviour is null. And fills the ref with the type when not
	/// </summary>
	/// <returns>The requested type if null.</returns>
	/// <param name="obj">object to check.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T CheckIfNull<T>(this T obj) where T: Component
	{
		if (obj == null)
		{
			//Find the correct type
			Component monoObject = Component.FindObjectOfType<T>();

			Debug.Log("<color=yellow>object of type " + monoObject.GetType().ToString() + " null</color>" );

			return monoObject as T;
		}

		return obj;
	}
}