﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Path segment.
/// </summary>
public class PathSegment {

	public Point firstPoint;

	public Point secondPoint;

	public Direction directionBetweenPoints;

	public int index;

	public PathSegment(Point A, Point B, int i)
	{
		firstPoint = A;
		secondPoint = B;
		directionBetweenPoints = DirectionCalculator.GetDirectionBetweenPoints((Vector2)A.transform.position, (Vector2)B.transform.position);
		index = i;
	}
}
