﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpriteRenderer))]
public class MainCharacterAnimation : MonoBehaviour {

	/// <summary>
	/// The animator.
	/// </summary>
	public Animator animator;

	/// <summary>
	/// The sprite renderer.
	/// </summary>
	public SpriteRenderer spriteRenderer;

	/// <summary>
	/// The path. Important that the chapterPath is not a part of this gameobject
	/// </summary>
	public ChapterPath path;

	/// <summary>
	/// The position to move to.
	/// </summary>
	private float tToMoveTo;

	/// <summary>
	/// The current t.
	/// </summary>
	private float currentT = 0;

	/// <summary>
	/// The smooth damp velocity.
	/// </summary>
	private float SmoothDampVelocity = 0f;

	/// <summary>
	/// The move.
	/// </summary>
	bool move = false;

	/// <summary>
	/// The smooth damp walk speed factor.
	/// </summary>
	public float SmoothDampVelocityToWalkSpeedFactor = 10.0f;

	/// <summary>
	/// The minimum distance to walk.
	/// </summary>
	public float minDistanceToWalk = 1;

	/// <summary>
	/// The smoothtime for the smoothdamp.
	/// </summary>
	public float smoothtime;

	/// <summary>
	/// The walk speed.
	/// </summary>
	private float walkSpeed;

	/// <summary>
	/// The motion curve.
	/// </summary>
	public AnimationCurve motionCurve;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake () 
	{
		//Safety check
		if (animator == null)
			animator = this.GetComponent<Animator>();

		if (spriteRenderer == null)
			spriteRenderer = this.GetComponent<SpriteRenderer>();

		if (path == null)
			Debug.Log("No path object added. Beware, make it a different gameobject than this");

		//Set intitial positions
		this.transform.position = path.GetPositionOnPath(0);

		//Default
		currentT = 0;
	}

	/// <summary>
	/// Sets the animation and transform. Uses the same t as the scrollcontroller path.
	/// </summary>
	/// <param name="t">T.</param>
	public void SetAnimationT(float t)
	{
		//The position for the character to move to
		tToMoveTo = motionCurve.Evaluate(t);

		//Activate the moving
		move = true;
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		//Walk or run to position
		if (move)
		{
			//Calculate the walkspeed
			walkSpeed = Mathf.Clamp01(Mathf.Abs(SmoothDampVelocity) * SmoothDampVelocityToWalkSpeedFactor);

			//Flip sprite if necessary
			CheckIfSpriteNeedsToBeFlipped();
		}

		//Set the walkspeed animation float
		animator.SetFloat("walkSpeed", walkSpeed);

	}

	/// <summary>
	/// A constant update function.
	/// </summary>
	void FixedUpdate () 
	{
		//Start moving
		if (move)
		{
			// In fixed update to remove jittering
			SetTransform();

			// calculate the distance
			float distance = Vector2.Distance(transform.position, path.GetPositionOnPath(tToMoveTo));

			//Set walking to false
			if (distance < minDistanceToWalk)
			{
				walkSpeed = 0;
				move = false;
			}
		}
	}

	/// <summary>
	/// Calculates the walkspeed.
	/// </summary>
	/// <returns>The walkspeed.</returns>
	void SetTransform()
	{
		//The current T of the animation 
		currentT = Mathf.SmoothDamp( currentT, tToMoveTo, ref SmoothDampVelocity, smoothtime);
			
		//convert this t to an position on the path
		this.transform.position = path.GetPositionOnPath(currentT);

		//declare variable
		float angle = 0;

		//Choose the angle to set the transform
		switch(path.GetCurrentPathSegment(currentT).directionBetweenPoints)
		{
			case Direction.down:
			angle = 270;
				break;
			case Direction.left:
			angle = 180;
				break;
			case Direction.up:
			angle = 90;
				break;
			case Direction.right:
			angle = 0;
				break;
		}

		//Set the angles
		this.transform.eulerAngles = new Vector3(0, 0, angle);
	}

	/// <summary>
	/// Checks if sprite needs to be mirrored.
	/// </summary>
	void CheckIfSpriteNeedsToBeFlipped()
	{
		//Move to the left
		if (currentT > tToMoveTo && spriteRenderer.flipX)
		{
			spriteRenderer.flipX = false;
		} 
		//Moving to the right
		else if (currentT < tToMoveTo && !spriteRenderer.flipX)
		{
			spriteRenderer.flipX = true;
		}
	}

	/// <summary>
	/// Previews the animation.
	/// </summary>
	/// <param name="t">T.</param>
	public void PreviewPosition(float t)
	{
		//Convert with curve
		t = motionCurve.Evaluate(t);

		//convert this t to an position on the path
		this.transform.position = path.GetPositionOnPath(t);
	}
}
