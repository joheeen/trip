﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

/// <summary>
/// Map controller.
/// </summary>
public class MapController : MonoBehaviour {

	/// <summary>
	/// The chapterview.
	/// </summary>
	public ChapterView chapterView;

	/// <summary>
	/// The map chapterbuttons.
	/// </summary>
	public MapChapterButton[] mapChapterbuttons;

	/// <summary>
	/// The fade in.
	/// </summary>
	public Fade fade;

	/// <summary>
	/// The loading panel.
	/// </summary>
	public GameObject loadingPanel;

	/// <summary>
	/// The current lvl.
	/// </summary>
	private int currentLvl;

	/// <summary>
	/// The navigation sprite.
	/// </summary>
	public GameObject navigationSprite;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
		//Nothing to loop through
		if (mapChapterbuttons == null || mapChapterbuttons.Length == 0)
			return;

		//Loop through all the found buttons
		foreach (MapChapterButton button in mapChapterbuttons)
		{
			//Add the event listener
			button.buttonPressed += MapChapterButtonPressed;
		}

		//Default state loading panel
		loadingPanel.SetActive(false);

		//Get the current lvl
		if (PlayerPrefs.HasKey("currentLvl"))
		{
			currentLvl = PlayerPrefs.GetInt("currentLvl");
		}
		else
		{
			currentLvl = 0;
		}

		//Activate the navigation sprite
		navigationSprite.SetActive(true);

		//Add eventlistener for the touch
		chapterView.touchMoved += OnTouchMoved;
	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		//Start animation
		fade.FadeIn();

		//Set the lvls active
		for (int i = 0; i <= currentLvl; i++)
		{
			mapChapterbuttons[i].SetSelectable();
		}
	}

	/// <summary>
	/// Raises the drag event.
	/// </summary>
	/// <param name="data">Data.</param>
	public void OnTouchMoved(Touch touch)
	{
		//Set the navigation sprite inactive on drag
		if (navigationSprite.activeSelf)
		{
			navigationSprite.SetActive(false);
		}
	}

	/// <summary>
	/// Invoke when the chapter button has been pressed.
	/// </summary>
	/// <param name="sender">Sender.</param>
	public void MapChapterButtonPressed(MapChapterButton sender)
	{
		//Check if scene name is not empty
		if ( sender.nameOfSceneToLoad != "" )
		{
			//Loop through all the found buttons
			foreach (MapChapterButton button in mapChapterbuttons)
			{
				//Remove the event listener
				button.buttonPressed -= MapChapterButtonPressed;
			}

			//
			Debug.Log("Loading scene: " + sender.nameOfSceneToLoad);

			//Set the loading panel active
			loadingPanel.SetActive(true);

			//Load the scene
			SceneManager.LoadScene(sender.nameOfSceneToLoad);
		}
		else
		{
			Debug.Log("Name of scene to load is empty");
		}
	}

	/// <summary>
	/// Raises the application quit event.
	/// </summary>
	public void OnDestroy()
	{
		//Loop through all the found buttons
		foreach (MapChapterButton button in mapChapterbuttons)
		{
			//Remove the event listener
			button.buttonPressed -= MapChapterButtonPressed;
		}

		//Remove eventlistener for the touch
		chapterView.touchMoved -= OnTouchMoved;
	}
}
