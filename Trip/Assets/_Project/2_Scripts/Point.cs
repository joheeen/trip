﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;

/// <summary>
/// Point
/// </summary>
[RequireComponent(typeof(Transform))]
[Serializable]
public class Point : MonoBehaviour
{
	#if UNITY_EDITOR

	/// <summary>
	/// The position moved UnityEvent.
	/// </summary>
	[SerializeField]
	public UnityEvent positionMoved;

	/// <summary>
	/// Sets the delegate. the Drawline method of the path is listening on the positionMoved Event
	/// </summary>
	public void SetUnityEvent(ChapterPath path)
	{
		//Add the DrawLine method to the delegate of the points
		UnityEditor.Events.UnityEventTools.AddPersistentListener(positionMoved, path.DrawLine);
		positionMoved.SetPersistentListenerState(0, UnityEventCallState.EditorAndRuntime);
	}

	/// <summary>
	/// Triggers the after move event.
	/// </summary>
	public void TriggerAfterMoveEvent()
	{
		//Invoke the positionMoved
		if (positionMoved != null)
			positionMoved.Invoke();
	}

	#endif
}
