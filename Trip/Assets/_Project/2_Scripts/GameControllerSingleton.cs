﻿using UnityEngine;
using System.Collections;

public class GameControllerSingleton : MonoBehaviour {

	/// <summary>
	/// The shared instance.
	/// </summary>
	public static GameControllerSingleton sharedInstance = null;

	/// <summary>
	/// Update event.
	/// </summary>
	public delegate void SelectableEventDelegate(ISelectable selectableObject);

	/// <summary>
	/// Texts the object clicked.
	/// </summary>
	/// <returns>The object clicked.</returns>
	public SelectableEventDelegate textObjectSelected;

	/// <summary>
	/// Texts the object deselected.
	/// </summary>
	/// <returns>The object deselected.</returns>
	public SelectableEventDelegate textObjectDeselected;

	/// <summary>
	/// The defense button selected.
	/// </summary>
	public SelectableEventDelegate defenseButtonSelected;

	/// <summary>
	/// The defense button deselected.
	/// </summary>
	public SelectableEventDelegate defenseButtonDeselected;

	/// <summary>
	/// The chapterbookmark button selected.
	/// </summary>
	public SelectableEventDelegate bookmarkButtonSelected;

	/// <summary>
	/// The chapterbookmark button deselected.
	/// </summary>
	public SelectableEventDelegate bookmarkButtonDeselected;

	/// <summary>
	/// Observation update delegate.
	/// </summary>
	public delegate void ObservationUpdateDelegate(Observation observation);

	/// <summary>
	/// The update event.
	/// </summary>
	public ObservationUpdateDelegate updateEvent;

	/// <summary>
	/// Game controller delegate.
	/// </summary>
	public delegate void GameControllerDelegate();

	/// <summary>
	/// All answers are correct.
	/// </summary>
	public GameControllerDelegate allAnswersAreCorrect;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
			//Check if instance already exists
		if (sharedInstance == null)
		{
			//if not, set instance to this
			sharedInstance = this;
		}
		//If instance already exists and it's not this:
		else if (sharedInstance != this)
		{
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);    

			Debug.Log("Multiple gameControllerSingletons in scene! Deleted extra");
		}

		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);
	}
		
	//Game Settings

	/// <summary>
	/// Observation lvl.
	/// </summary>
	public enum ObservationLvl
	{
		fase1,
		fase2
	}

	/// <summary>
	/// The observation lvl used during the game.
	/// </summary>
	public ObservationLvl observationLvl;


}
