﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SegmentAnimationGroup : MonoBehaviour {

	/// <summary>
	/// The segment animation prefab.
	/// </summary>
	public GameObject segmentAnimationPrefab;

	/// <summary>
	/// The segment animations.
	/// </summary>
	[SerializeField]
	public List<SegmentAnimation> segmentAnimations;

	#if UNITY_EDITOR

	/// <summary>
	/// Adds a segment animation to the list.
	/// </summary>
	public void AddSegmentAnimation()
	{
		//check
		if (segmentAnimations == null)
			segmentAnimations = new List<SegmentAnimation>();

		//Store the animation ref
		GameObject segmentAnimationGameObject = GameObject.Instantiate(segmentAnimationPrefab);

		//Add transform as child of this
		segmentAnimationGameObject.transform.parent = this.transform;

		//Set the name of the animation object
		segmentAnimationGameObject.name = "segmentAnimation_" + segmentAnimations.Count.ToString();

		//Store the point script
		SegmentAnimation segmentAnimation = segmentAnimationGameObject.GetComponent<SegmentAnimation>();

		//Add to pointslist
		segmentAnimations.Add(segmentAnimation);
	}

	/// <summary>
	/// Deletes the animation.
	/// </summary>
	/// <param name="index">Index.</param>
	public void DeleteAnimation(int index)
	{
		if (segmentAnimations.Count == 0 || segmentAnimations == null)
		{
			Debug.Log("list already empty, or no list");
			return;
		}

		//Destroy object
		DestroyImmediate(segmentAnimations[index].gameObject);

		//Delete from points list
		segmentAnimations.RemoveAt(index);
	}

	/// <summary>
	/// Sets to position in the editor.
	/// </summary>
	/// <param name="position">Position.</param>
	public void PreviewAnimations(float t)
	{
		//preview the animations in the group
		foreach (SegmentAnimation segmentAnimation in segmentAnimations)
		{
			//Preview the animation
			if (segmentAnimation.sprites != null && segmentAnimation.sprites.Count != 0)
			segmentAnimation.PreviewAnimation(t);
		}
	}

	#endif

	/// <summary>
	/// Sets the animations t.
	/// </summary>
	public void SetAnimationsT(float scrollT)
	{
		//play the animations in the group
		foreach (SegmentAnimation segmentAnimation in segmentAnimations)
		{
			if (segmentAnimation.sprites != null && segmentAnimation.sprites.Count != 0)
			segmentAnimation.SetAnimationT(scrollT);
		}
	}
}
