﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Chapter observation controller manages all the observations in the scene like linking textobjects.
/// </summary>
public class ChapterObservationController : MonoBehaviour {

	/// <summary>
	/// The observation mini menu.
	/// </summary>
	public ObservationMiniMenu observationMiniMenu;

	/// <summary>
	/// The observations.
	/// </summary>
	List<ChapterObservation> observations;

	/// <summary>
	/// The textobject clicked for linking.
	/// </summary>
	ChapterTextObject textobjectClickedForLinking;

	/// <summary>
	/// The link modus.
	/// </summary>
	bool linkModus = false;

	/// <summary>
	/// The observation line material.
	/// </summary>
	public Material ObservationLineMaterial;

	/// <summary>
	/// The available line colors.
	/// </summary>
	public List<GroupColor> availableGroupColors;

	/// <summary>
	/// Observation controller delegate.
	/// </summary>
	public delegate void ObservationControllerDelegate(List<ChapterObservation> observations);

	/// <summary>
	/// Occurs when observations update.
	/// </summary>
	public event ObservationControllerDelegate observationsUpdate;

	/// <summary>
	/// The result panel.
	/// </summary>
	public ResultPanel resultPanel;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Start()
	{
		//Set the event listeners for the text objects
		observationMiniMenu.ChoiceMadeEventNotNone += ChoiceMadeOnTextObject;
		observationMiniMenu.LinkButtonPressedEvent += LinkButtonPressed;
		GameControllerSingleton.sharedInstance.updateEvent += UpdateObservation;
	}

	/// <summary>
	/// Texts object clicked. Checks if there needs to be linkin
	/// </summary>
	/// <param name="selectedTextObject">Selected text object.</param>
	public void TextObjectClicked(ChapterTextObject selectedTextObject)
	{
		//Moved to the link button pressed event
	}

	/// <summary>
	/// Stores the selected text object when a choice is made with the miniMenu
	/// </summary>
	/// <param name="textObject">Text object.</param>
	void ChoiceMadeOnTextObject (ChapterTextObject selectedTextObject)
	{
		//Not enough group colors
		if (observations != null)
		{
			if (observations.Count >= 8)
			{
				Debug.Log("Not enough group Colors");
				return;
			}
		}
		//Selected object is not linked yet
		if (selectedTextObject.linkedToObservation == null)
		{
			CreateAndStoreObservation(selectedTextObject);
		}
		//Selected object is linked to a observation but a new choice has been made
		else
		{
			//something when the linked buttons have the same choice
			selectedTextObject.linkedToObservation.CheckIfButtonsHaveSameChoiceAndRemoveChoiceIfTrue(selectedTextObject, false);
		}
			
		//Invoke event
		observationsUpdate(observations);
	}

	/// <summary>
	/// The link button is pressed.
	/// </summary>
	void LinkButtonPressed(ChapterTextObject selectedTextObject)
	{
		//First time link button pressed
		if (!linkModus || textobjectClickedForLinking == null)
		{
			//store the textObject where the linkButton has been pressed
			textobjectClickedForLinking = selectedTextObject;

			//Activate linkmodus
			linkModus = true;

			//Set linkbutton sprite
			observationMiniMenu.linkButton.Select();
		}
		//Linking if possible
		else
		{
			//Check if it is not the same button
			if (selectedTextObject != textobjectClickedForLinking)
			{
				//The selected text is already linked
				if (selectedTextObject.linkedToObservation != null)
				{
					//Do nothing if correct
					if (selectedTextObject.linkedToObservation.correct)
					{
						Debug.Log("Trying to link to textobject with correct observation. Not possible");
						return;
					}
					else
					{
						//Remove the reference
						selectedTextObject.linkedToObservation.RemoveTextObjectFromObservationAndReference(selectedTextObject);
					}
				}

				//Check if the textObject that clicked has a observation
				if (textobjectClickedForLinking.linkedToObservation == null)
				{
					//Add observation
					CreateAndStoreObservation(textobjectClickedForLinking);
				}

				//Link selectedText object to the same observation
				textobjectClickedForLinking.linkedToObservation.AddTextObjectToObservationAndSetAsReference(selectedTextObject);
			}
			else
			{
				Debug.Log("Same button pressed for linking. not possible");
			}

			//Return to default
			textobjectClickedForLinking = null;
			linkModus = false;

			if (observations != null)
			{
				//Invoke event
				observationsUpdate(observations);
			}
		}
	}

	/// <summary>
	/// Deletes the observation.
	/// </summary>
	/// <param name="observation">Observation.</param>
	public void DeleteObservation(ChapterObservation observation)
	{
		//There are observations
		if (observations != null && observations.Count != 0)
		{
			//Remove the listener
			observation.ObservationIsDeprecated -= DeleteObservation;

			//Return the color of the observation
			availableGroupColors.Add(observation.groupColor);

			//Invoke the removed event
			resultPanel.RemoveBookmark(observation);

			//remove observation
			observations.Remove(observation);
		} 
		else
		{
			Debug.Log("Error: No observations in or list");
		}

		if (observations != null)
		{
			//Invoke event
			observationsUpdate(observations);
		}
	}

	/// <summary>
	/// Updates the observation.
	/// </summary>
	/// <param name="observation">Observation.</param>
	void UpdateObservation(Observation observation)
	{
		//TODO: BUG here
		//The observation has been changed. Check again for answers
		observationsUpdate(observations);
	}

	/// <summary>
	/// Creates the and store observation.
	/// </summary>
	/// <param name="selectedTextObject">Selected text object.</param>
	public void CreateAndStoreObservation(ChapterTextObject selectedTextObject)
	{
		if (observations == null)
			observations = new List<ChapterObservation>();

		//Pick color to add to the line
		GroupColor groupColor = availableGroupColors[Random.Range(0, availableGroupColors.Count - 1)];
		availableGroupColors.Remove(groupColor);

		//Create observation
		ChapterObservation observation = new ChapterObservation(ObservationLineMaterial, groupColor);

		//Add reference
		observation.AddTextObjectToObservationAndSetAsReference(selectedTextObject);

		//Add event listener to delete when empty
		observation.ObservationIsDeprecated += DeleteObservation;

		//Add to list
		observations.Add(observation);

		//Invoke event for adding the bookmark
		resultPanel.AddBookmark(observation);
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy()
	{
		//Remove the event listeners for the text objects
		observationMiniMenu.ChoiceMadeEventNotNone -= ChoiceMadeOnTextObject;
		observationMiniMenu.LinkButtonPressedEvent -= LinkButtonPressed;
		GameControllerSingleton.sharedInstance.updateEvent -= UpdateObservation;
	}
		
}
