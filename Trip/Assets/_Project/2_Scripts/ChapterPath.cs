﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System;

[ExecuteInEditMode]
[RequireComponent(typeof(LineRenderer))]
[Serializable]
public class ChapterPath : MonoBehaviour 
{

	/// <summary>
	/// The point prefab.
	/// </summary>
	public GameObject pointPrefab;

	/// <summary>
	/// The spawn position offset.
	/// </summary>
	public Vector3 spawnPositionOffset;

	/// <summary>
	/// Deactivate linerender in runtime.
	/// </summary>
	public bool deactivateLinerenderInRuntime = true;

	///	<summary>
	/// The list of vector2 points to draw the line between.
	/// </summary>
	[SerializeField]
	public List<Point> points = new List<Point>();

	/// <summary>
	/// The lineRenderer visualizing the line that represents the path.
	/// </summary>
	/// <value>
	/// Getter checks if line == null and always returns the correct value.
	/// </value>
	public LineRenderer line {
		get {
			if (_line == null)
			{
				_line = this.GetComponent<LineRenderer>();
			}

			return _line;
		}
	}

	/// <summary>
	/// The private lineRenderer. Used as the return value in the Line Getter. Don't use this one.
	/// </summary>
	private LineRenderer _line;

	/// <summary>
	/// Checks if the length has already been calculated. Returns the value.
	/// </summary>
	/// <value>The length of the path.</value>
	public float length {
		get {
			if (_length <= 0f)
			{
				if (points.Count <= 1)
				{
					Debug.Log("Not enough points in path");
					return 0f;
				}
				else
				{
					_length = 0;

					for (int i = 1; i < points.Count; i++)
					{
						_length += Vector2.Distance((Vector2)points[i-1].transform.position, (Vector2)points[i].transform.position);
					}

					return _length;
				}
			} 
			else 
			{
				return _length;
			}
		}
	}
	private float _length;

	/// <summary>
	/// The segments of the path.
	/// </summary>
	public PathSegment[] segments {
		get 
		{
			if (_segments == null || _segments.Length == 0)
			{
				_segments = collectSegments();
				return _segments;
			}
			else
			{
				return _segments;
			}
		}
	}
	private PathSegment[] _segments;

	/// <summary>
	/// Collects the segments.
	/// </summary>
	/// <returns>The segments.</returns>
	PathSegment[] collectSegments()
	{
		PathSegment[] _segments = new PathSegment[points.Count - 1];
		for (int i = 1; i < points.Count; i++)
		{
			_segments[i-1] = new PathSegment(points[i-1], points[i], i-1);
		}
		return _segments;
	}
		
	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		//Hide linerender if true
		if (deactivateLinerenderInRuntime)
			line.enabled = false;
	}

	#if UNITY_EDITOR

	/// <summary>
	/// Instantiates an Point GameObject and adds a point to path.
	/// </summary>
	public void CreateAndAddPointToPath() 
	{
		//Store the point ref
		GameObject pointGameObject = GameObject.Instantiate(pointPrefab);

		//Set the position of the point to the most recent point
		if (points != null && points.Count != 0)
		{
			pointGameObject.transform.position = points[points.Count - 1].gameObject.transform.position + spawnPositionOffset;
		}
		else
		{
			//First point
			pointGameObject.transform.position = this.transform.position;
		}

		//Add transform as child of this
		pointGameObject.transform.parent = this.transform;

		//Set the name of the point
		pointGameObject.name = "point_" + points.Count.ToString();

		//Store the point script
		Point point = pointGameObject.GetComponent<Point>();

		//Set the UnityEvent
		point.SetUnityEvent(this);

		//Add to pointslist
		points.Add(point);
	}

	/// <summary>
	/// Deletes the last point.
	/// </summary>
	public void DeleteLastPoint()
	{
		if (points.Count == 0 || points == null)
		{
			Debug.Log("list already empty, or no list");
			return;
		}

		//Destroy object
		DestroyImmediate(points[points.Count - 1].gameObject);

		//Delete from points list
		points.RemoveAt(points.Count - 1);
	}

	/// <summary>
	/// Deletes the points list and the gameobjects
	/// </summary>
	public void DeletePoints()
	{
		if (points.Count == 0 || points == null)
		{
			Debug.Log("list already empty, or no list");
			return;
		}

		for( int i = 0; i < points.Count; i++)
		{
			if (points[i] != null)
			{
				DestroyImmediate(points[i].gameObject);
			}
		}

		//Remove the drawing
		line.SetVertexCount(1);

		//Reset to default
		points.Clear();
	}

	/// <summary>
	/// Draws the line.
	/// </summary>
	public void DrawLine() 
	{
		//Enable line if it's false
		if (!line.enabled)
			line.enabled = true;
		
		//Set the vertexes for the linerenderer
		line.SetVertexCount(points.Count);

		//Draw the line
		for( int i = 0; i < points.Count; i++ )
		{
			line.SetPosition(i, (Vector2)points[i].transform.position);
		}
	}
		
	#endif

//	/// <summary>
//	/// Awake this instance.
//	/// </summary>
//	public void Awake()
//	{
//		//Disable the visual line
//		line.enabled = false;
//	}
//
//	/// <summary>
//	/// Awake this instance.
//	/// </summary>
//	public void OnApplicationQuit()
//	{
//		//Enable the visual line
//		line.enabled = true;
//	}

	/// <summary>
	/// Gets the position on path.
	/// </summary>
	/// <returns>The position on path.</returns>
	/// <param name="t">T Needs to be a number between 0 and 1.</param>
	public Vector2 GetPositionOnPath(float t)
	{
		//Clamp the value between 0 - 1
		t = Mathf.Clamp01(t);

		//Get the lenght where the position needs to be
		float positionAtLength = t * length;

		//The length used in the for loop
		float currentLength = 0f;
		float previousLength = 0f;

		//Loop through the points in the path
		for (int i = 1; i < points.Count; i++)
		{
			float distanceBetweenPoints = Vector2.Distance((Vector2)points[i-1].transform.position, (Vector2)points[i].transform.position);
			currentLength += distanceBetweenPoints;

			//Check if the currentLenght is more than te requested position length
			if (currentLength > positionAtLength)
			{
				float remainingLength = positionAtLength - previousLength;
				return Vector2.Lerp((Vector2)points[i-1].transform.position, (Vector2)points[i].transform.position, remainingLength / distanceBetweenPoints);
			}

			previousLength = currentLength;
		}

		//Else t == 1
		return (Vector2)points[points.Count - 1].transform.position;
	}


	public Vector2 GetPositionOnPath(float t, ref PathSegment currentSegment, ref PathSegment nextSegment, ref PathSegment previousSegment)
	{
		//Clamp the value between 0 - 1
		t = Mathf.Clamp01(t);

		//Get the lenght where the position needs to be
		float positionAtLength = t * length;

		//The length used in the for loop
		float lengthToCurrentIndex = 0f;
		float lengthToPreviousIndex = 0f;

		//Loop through the points in the path
		for (int i = 1; i < points.Count; i++)
		{
			float distanceBetweenPoints = Vector2.Distance((Vector2)points[i-1].transform.position, (Vector2)points[i].transform.position);
			lengthToCurrentIndex += distanceBetweenPoints;

			//Check if the currentLenght is more than te requested position length
			if (lengthToCurrentIndex > positionAtLength)
			{
				//Setting the currentSegment
				currentSegment = segments[i - 1];

				//Setting the previousSegment
				if (currentSegment.index > 0)
				{
					previousSegment = segments[i - 2];
				} 
				else
				{
					previousSegment = null;
				}

				//Setting the nextSegment
				if (currentSegment.index < segments.Length - 1)
				{
					nextSegment = segments[i];
				}
				else
				{
					nextSegment = null;
				}

//				Debug.Log(previousSegment.index.ToString() + currentSegment.index.ToString() + nextSegment.index.ToString());

				//Calculate the remaining lenth
				float remainingLength = positionAtLength - lengthToPreviousIndex;

				//Return the position
				return Vector2.Lerp((Vector2)points[i-1].transform.position, (Vector2)points[i].transform.position, remainingLength / distanceBetweenPoints);
			}

			lengthToPreviousIndex = lengthToCurrentIndex;
		}

		//Else t == 1
		return (Vector2)points[points.Count - 1].transform.position;
	}

	/// <summary>
	/// Gets the current path segment.
	/// </summary>
	/// <returns>The current path segment.</returns>
	/// <param name="t">T.</param>
	public PathSegment GetCurrentPathSegment(float t)
	{
		//Clamp the value between 0 - 1
		t = Mathf.Clamp01(t);

		//Get the lenght where the position needs to be
		float positionAtLength = t * length;

		//The length used in the for loop
		float lengthToCurrentIndex = 0f;
		float lengthToPreviousIndex = 0f;

		//The current path segment
		PathSegment currentSegment;

		//Loop through the points in the path
		for (int i = 1; i < points.Count; i++)
		{
			float distanceBetweenPoints = Vector2.Distance((Vector2)points[i-1].transform.position, (Vector2)points[i].transform.position);
			lengthToCurrentIndex += distanceBetweenPoints;

			//Check if the currentLenght is more than te requested position length
			if (lengthToCurrentIndex > positionAtLength)
			{
				//Setting the currentSegment
				currentSegment = segments[i - 1];

				//Return the position
				return currentSegment;
			}

			lengthToPreviousIndex = lengthToCurrentIndex;
		}

		//Else t == 1
		return segments[segments.Length - 1];
	}
}