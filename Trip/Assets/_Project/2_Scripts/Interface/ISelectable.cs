﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Iselectable interface.
/// </summary>
public interface ISelectable {

	/// <summary>
	/// Raises the select event.
	/// </summary>
	void OnSelect();

	/// <summary>
	/// Raises the deselect event.
	/// </summary>
	void OnDeselect();
}
