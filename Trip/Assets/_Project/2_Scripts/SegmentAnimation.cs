﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
[System.Serializable]
public class SegmentAnimation : MonoBehaviour {

	/// <summary>
	/// The start t.
	/// </summary>
	[Range(0f, 1f)]
	public float startT = 0f;

	/// <summary>
	/// The end t.
	/// </summary>
	[Range(0f, 1f)]
	public float endT = 0.1f;

	/// <summary>
	/// The t in middle position.
	/// </summary>
	[Range(0f, 0.5f)]
	public float tInMiddlePosition;

	/// <summary>
	/// The start position.
	/// </summary>
	public Vector2 startPosition;

	/// <summary>
	/// The middle position.
	/// </summary>
	public Vector2 middlePosition;

	/// <summary>
	/// The end position.
	/// </summary>
	public Vector2 endPosition;

	/// <summary>
	/// The sprites for the animation.
	/// </summary>
	public List<Sprite> sprites;

	/// <summary>
	/// The sprite renderer.
	/// </summary>
	private SpriteRenderer spriteRenderer;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
		//initialise sprite renderer
		spriteRenderer = this.GetComponent<SpriteRenderer>();

		//Set the first sprite
		if (sprites != null && sprites.Count != 0)
		{
			spriteRenderer.sprite = sprites[0];
		}
	}

	/// <summary>
	/// Sets the animation float from the T of the scroll. Uses own starting t and end t for animation.
	/// </summary>
	/// <param name="scrollT">Scroll t.</param>
	public void SetAnimationT (float scrollT) 
	{
		//Check if there is an animation
		if (sprites == null || sprites.Count == 0)
			return;

		//When the camera scroll is within the start and end t for this animation
		if (scrollT > startT && scrollT < endT)
		{
			//Set the T for the animation
			float animationT = (scrollT - startT) / (endT - startT);

			//Play sprite animation
			PlaySpriteAnimation(animationT);

			//Set transform
			UpdateAnimationTransform(animationT);
		}
	}

	/// <summary>
	/// Sets the animation.
	/// </summary>
	/// <param name="animationT">Animation t.</param>
	private void PlaySpriteAnimation(float animationT)
	{
		//Get the current index
		int index = Mathf.FloorToInt((sprites.Count - 1) * animationT);

		//Set the correct sprite
		spriteRenderer.sprite = sprites[index];
	}

	/// <summary>
	/// Sets the animation transform.
	/// </summary>
	/// <param name="animationT">Animation t.</param>
	private void UpdateAnimationTransform(float animationT)
	{
		//Animation position lerp to middle
		if (animationT <= 0.5f - (tInMiddlePosition / 2f))
		{
			Vector2 positionToMoveTo = Vector2.Lerp(startPosition, middlePosition, animationT / (0.5f - (tInMiddlePosition / 2f)));
			gameObject.transform.position = new Vector3(positionToMoveTo.x, positionToMoveTo.y, gameObject.transform.position.z);
		} 
		else if (animationT >= 0.5f + (tInMiddlePosition / 2f))//From middle to end
		{
			Vector2 positionToMoveTo = Vector2.Lerp(middlePosition, endPosition, (animationT - (0.5f + (tInMiddlePosition / 2f))) / (0.5f - (tInMiddlePosition / 2f)));
			gameObject.transform.position = new Vector3(positionToMoveTo.x, positionToMoveTo.y, gameObject.transform.position.z);
		}
	}


	#if UNITY_EDITOR

	/// <summary>
	/// Sets to position in the editor.
	/// </summary>
	/// <param name="position">Position.</param>
	public void PreviewAnimation(float t)
	{
		//Sprite renderer
		if (spriteRenderer == null)
		{
			spriteRenderer = this.GetComponent<SpriteRenderer>();
		}

		//Initial value
		float previewT = 0;

		//When the camera scroll is within the start and end t for this animation
		if (t > startT && t < endT)
		{
			//Set the T for the animation
			previewT = (t - startT) / (endT - startT);
		} 
		else if (t > endT)
		{
			//Max value
			previewT = 1;
		}

		//Play sprite animation
		PlaySpriteAnimation(previewT);

		//Set transform
		UpdateAnimationTransform(previewT);
	}

	#endif
}
