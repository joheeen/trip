﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class EndAnimationButton : MonoBehaviour, ISelectable {

	/// <summary>
	/// The panel to activate on press.
	/// </summary>
	public GameObject panelToActivateOnPress;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	public void Awake()
	{
		//Add the box collider at awake. This way it matches the size perfectly
		BoxCollider collider = this.gameObject.AddComponent<BoxCollider>();

		//Deactivate Panel
		panelToActivateOnPress.SetActive(false);
	}

	/// <summary>
	/// Raises the pointer exit event.
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnSelect ()
	{
		//Set animationPanelActive
		panelToActivateOnPress.SetActive(true);
	}

	public void OnDeselect ()
	{
//		panelToActivateOnPress.SetActive(false);
	}
}
