﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Disclaimer controller.
/// </summary>
public class DisclaimerController : MonoBehaviour {

	/// <summary>
	/// The chapter view.
	/// </summary>
	public ChapterView chapterView;

	/// <summary>
	/// The scroll controller.
	/// </summary>
	public ChapterScrollController scrollController;

	/// <summary>
	/// The fade image.
	/// </summary>
	public Fade fade;

	/// <summary>
	/// The accepted button sprite.
	/// </summary>
	public Sprite acceptedButtonSprite;

	/// <summary>
	/// The accepted bookmark sprite.
	/// </summary>
	public Sprite acceptedBookmarkSprite;

	/// <summary>
	/// The circle accepted sprite.
	/// </summary>
	public Sprite circleAcceptedSprite;

	/// <summary>
	/// The disclaimer accept button image.
	/// </summary>
	public SpriteRenderer disclaimerAcceptButtonSpriteRenderer;

	/// <summary>
	/// The bookmark sprite renderer.
	/// </summary>
	public SpriteRenderer bookmarkSpriteRenderer;

	/// <summary>
	/// The circle sprite renderer.
	/// </summary>
	public SpriteRenderer circleSpriteRenderer;

	/// <summary>
	/// Raises the awake event.
	/// </summary>
	void Awake()
	{
		//Set chapter events
		chapterView.touchMoved += TouchOnViewMoved;
		chapterView.touchBegan += TouchOnViewBegan;
	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		//Start animation
		fade.FadeIn();
	}

	/// <summary>
	/// Touchs the on view began.
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchOnViewBegan(Touch touch)
	{
		//Initialise ray
		Ray ray = Camera.main.ScreenPointToRay(touch.position);
		RaycastHit hit = new RaycastHit();

		//Cast ray
		if (Physics.Raycast(ray, out hit, 100))
		{
			//Button has been selected
			if (hit.transform.tag == "Button")
			{
				disclaimerAccepted();
			}
		}
	}

	/// <summary>
	/// Touchs the on view moved. :-p Derp
	/// </summary>
	/// <param name="touch">Touch.</param>
	void TouchOnViewMoved(Touch touch)
	{
		//End of scene
		if (scrollController.t == 1)
		{
			//Load the mapscene
			SceneManager.LoadScene("Map_Scene");
		}
	}

	/// <summary>
	/// The disclaimer has been accepted.
	/// </summary>
	void disclaimerAccepted()
	{
		//Set new sprites
		disclaimerAcceptButtonSpriteRenderer.sprite = acceptedButtonSprite;

		bookmarkSpriteRenderer.sprite = acceptedBookmarkSprite;

		circleSpriteRenderer.sprite = circleAcceptedSprite;

		//Set show finish to true
		scrollController.OverrideShowFinishToTrue();
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy()
	{
		chapterView.touchMoved += TouchOnViewMoved;
	}
}
