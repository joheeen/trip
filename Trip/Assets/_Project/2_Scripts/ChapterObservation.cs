﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Chapter observation.
/// </summary>
public class ChapterObservation: Observation 
{
	/// <summary>
	/// The text objects.
	/// </summary>
	private List<ChapterTextObject> textObjects;

	/// <summary>
	/// Observation event.
	/// </summary>
	public delegate void ObservationEvent(ChapterObservation self);

	/// <summary>
	/// Occurs when observation is deprecated.
	/// </summary>
	public event ObservationEvent ObservationIsDeprecated;

	/// <summary>
	/// The line renderer.
	/// </summary>
	LineRenderer lineRenderer;

	/// <summary>
	/// The game object.
	/// </summary>
	private GameObject gameObject;

	/// <summary>
	/// The color of the line.
	/// </summary>
	public Color color;

	/// <summary>
	/// The color of the group.
	/// </summary>
	public GroupColor groupColor;

	/// <summary>
	/// The correct.
	/// </summary>
	public bool correct = false;

	/// <summary>
	/// The defense.
	/// </summary>
	public Defense defense 
	{
		get 
		{	
			return _defense;
		}
	}
	private Defense _defense;

	/// <summary>
	/// The instantiated line renderer objects.
	/// </summary>
	private List<GameObject> instantiatedLineRendererObjects; 

//	/// <summary>
//	/// Initializes a new instance of the <see cref="ChapterObservation"/> class.
//	/// </summary>
	public ChapterObservation(Material lineRendererMaterial, GroupColor GroupColor)
	{
		//Create an empty gameobject
		gameObject = new GameObject("Observation object");

		//Add linerenderer and set properties
		lineRenderer = gameObject.AddComponent<LineRenderer>();
		lineRenderer.material = new Material(lineRendererMaterial.shader);
		lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		lineRenderer.useLightProbes = false;
		lineRenderer.receiveShadows = false;
		lineRenderer.SetWidth(1f, 1f);
		color = GroupColor.color;
		groupColor = GroupColor;
		lineRenderer.SetColors(color, color);

		//Initialise
		instantiatedLineRendererObjects = new List<GameObject>();
	}
		
	/// <summary>
	/// Adds the text object to observation and sets this observation as reference.
	/// </summary>
	public void AddTextObjectToObservationAndSetAsReference(ChapterTextObject textObject)
	{
		//Check if textObjects is null
		if (textObjects == null)
			textObjects = new List<ChapterTextObject>();

		//textObjects doesn't contain the same
		if (!textObjects.Contains(textObject))
		{
			textObjects.Add(textObject);
			textObject.linkedToObservation = this;
			Debug.Log("succesfully added textobject");
		}
		else 
		{
			//Already in list
			Debug.Log("List already contained textobject");
		}

		//TextObjects are linked
		if (textObjects.Count > 1)
		{
			Debug.Log("Connection between textObjects");

			//Check if buttons have same choice and set to none if true
			CheckIfButtonsHaveSameChoiceAndRemoveChoiceIfTrue(textObject, true);

			//Draw the line between the points
			DrawLineBetweenLinkedTextObjects();
		}
		else //one object left
		{
			lineRenderer.enabled = false;
		}

		//TODO: Use the GameManagerSingleton to check if the maximum linked text amount is not exceeded
	}

	/// <summary>
	/// Adds the text object to observation and sets this observation as reference.
	/// </summary>
	public void RemoveTextObjectFromObservationAndReference(ChapterTextObject textObject)
	{
		//Check if textObjects is null
		if (textObjects == null || textObjects.Count == 0)
		{
			Debug.Log("No text objects stored in this Observation");
			return;
		}

		//textObjects doesn't contain the same
		if (!textObjects.Contains(textObject))
		{
			Debug.Log("textobject not in this observation");
		}
		else if (textObject.linkedToObservation != this)
		{
			Debug.Log("textobject not linked to this observation");
		}
		else 
		{
			//Exists in this observation and is linked
			textObject.linkedToObservation = null;

			//Reset to unlinked color
			textObject.GetComponent<TextMesh>().color = Color.black;

			textObjects.Remove(textObject);
			Debug.Log("textobject succesfully removed from observation and unlinked");

			//Set color of the last linked text object back to default
			if (textObjects.Count == 1)
			{
				//Return to default color
				textObjects[0].GetComponent<TextMesh>().color = Color.black;
			}
			//No textObjects left in observation
			else if (textObjects.Count == 0)
			{
				//Destroy this gameobject
				GameObject.Destroy(gameObject);

				//Controller handles the rest
				ObservationIsDeprecated(this);
			}
		}

		//Still connections active
		if (textObjects.Count > 1)
		{
			//Draw the line between the points
			DrawLineBetweenLinkedTextObjects();
		}
		else //one object left
		{
			lineRenderer.enabled = false;
		}
	}

	/// <summary>
	/// Checks if buttons have same choice and remove choice if true.
	/// </summary>
	/// <param name="selectedTextObject">Selected text object.</param>
	public void CheckIfButtonsHaveSameChoiceAndRemoveChoiceIfTrue(ChapterTextObject selectedTextObject, bool resetSelectedObject)
	{
		//No need for checking if there is no choice
		if (selectedTextObject.GetChoice() == MiniMenuChoice.None)
			return;
		
		//Check if there are enough textobjects
		if (textObjects.Count > 1 && textObjects != null)
		{
			foreach (ChapterTextObject text in textObjects)
			{
				//Don't compare to same object AND choice is same as other
				if (text != selectedTextObject && text.GetChoice() == selectedTextObject.GetChoice())
				{
					if (resetSelectedObject)
					{
						//Reset selectedTextObject to none
						selectedTextObject.SetChoice(MiniMenuChoice.None);
					}
					else
					{
						//Reset other text object to none
						text.SetChoice(MiniMenuChoice.None);
					}
				}
			}
		}
		else
			Debug.Log("Not enough text");
	}

	/// <summary>
	/// Draws the line between linked text objects.
	/// </summary>
	public void DrawLineBetweenLinkedTextObjects()
	{
		Debug.Log("Draw lines from observation");

		//Linerender is disabled
		if (!lineRenderer.enabled)
			lineRenderer.enabled = true;

		//Delete the previous Linerender Objects
		if (instantiatedLineRendererObjects.Count != 0)
		{
			foreach(GameObject gameObject in instantiatedLineRendererObjects)
			{
				//Destroy these gameobjects
				GameObject.Destroy(gameObject);
			}
		}

		//The reference to use
		LineRenderer lineRendererReference;

		//Loop through the textobject and set the linerender positions
		for (int i = 1; i < textObjects.Count; i++)
		{
			//Use the linerenderer on this object
			if (i == 1)
			{
				lineRendererReference = lineRenderer;
			} 
			//Use a complete new linerenderer object
			else
			{
				//Instantiate new object and set the component and material
				GameObject gameObject = new GameObject("Observation linereneder " + i.ToString());
				lineRendererReference = gameObject.AddComponent<LineRenderer>();
				lineRendererReference.material = lineRenderer.material;
				lineRendererReference.SetColors(color, color);
				lineRendererReference.useLightProbes = false;
				lineRendererReference.receiveShadows = false;
				lineRendererReference.SetWidth(1f, 1f);

				//Add the reference
				instantiatedLineRendererObjects.Add(gameObject);
			}

			//Set the vertex count
			lineRendererReference.SetVertexCount(2);

			//Set the positions of the linerender + z axis offset to set the line behind the text. fixed number because positions vary based on selected or not
			lineRendererReference.SetPosition(0, new Vector3(textObjects[i - 1].stampSpriteRenderer.gameObject.transform.position.x, textObjects[i - 1].stampSpriteRenderer.gameObject.transform.position.y, 3));

			//Set the positions of the linerender + z axis offset to set the line behind the text. fixed number because positions vary based on selected or not
			lineRendererReference.SetPosition(1, new Vector3(textObjects[i].stampSpriteRenderer.gameObject.transform.position.x, textObjects[i].stampSpriteRenderer.gameObject.transform.position.y, 3));

			//Calculate the distance
			float distance = Vector2.Distance(textObjects[i - 1].stampSpriteRenderer.gameObject.transform.position, textObjects[i].stampSpriteRenderer.gameObject.transform.position);

			//Set the distance for the dots with correct distance
			lineRendererReference.material.SetFloat("_RepeatCount", Mathf.Round(distance * (45f/70f)));
		}
	}

	public List<ChapterTextObject> GetOptionalTextObjects ()
	{
		if (textObjects != null && textObjects.Count != 0)
		{
			return textObjects;
		}
		else
		{
			return null;
		}
	}


	public void SetDefense(Defense Defense)
	{
		//Set defense
		_defense = Defense;

		//Invoke update event
		GameControllerSingleton.sharedInstance.updateEvent(this);
	}

}
