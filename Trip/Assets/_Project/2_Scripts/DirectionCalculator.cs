﻿using UnityEngine;
using System.Collections;

public static class DirectionCalculator {

	/// <summary>
	/// Gets the swipe direction.
	/// </summary>
	/// <returns>The swipe direction.</returns>
	/// <param name="touchDelta">Touch delta.</param>
	public static Direction GetDirectionBetweenPoints(Vector2 firstPosition, Vector2 secondPosition)
	{
		Vector2 vectorOfPoints = secondPosition - firstPosition;

		//Get the angle
		float angle = vectorOfPoints.y < 0 ? Vector2.Angle(Vector2.right, vectorOfPoints) : 360 - Vector2.Angle(Vector2.right, vectorOfPoints);

		//Default
		Direction direction = Direction.up;

		//up
		if (angle >= 225 && angle < 315)
		{
			direction = Direction.up;
		}
		//down
		if (angle >= 45 && angle < 135)
		{
			direction = Direction.down;
		}
		//left
		if (angle >= 135 && angle < 225)
		{
			direction = Direction.left;
		}
		//right
		if (angle >= 315 || angle < 45)
		{
			direction = Direction.right;
		}

		return direction;
	}
}

/// <summary>
/// The direction of a swipe.
/// </summary>
public enum Direction
{
	up,
	down,
	right,
	left
}
