﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class sizeController : MonoBehaviour {

	/// <summary>
	/// The size of the factor greater than screen.
	/// </summary>
	public float factorGreaterThanScreenSize;

	/// <summary>
	/// The sprite renderer.
	/// </summary>
	public Image image;

	void Awake()
	{
		if (image == null)
			image = this.GetComponent<Image>();
	}

	// Use this for initialization
	void Start () 
	{
		//Get size for the sprite
		float newWidth = Screen.width * factorGreaterThanScreenSize;
		float newHeight = (newWidth / image.rectTransform.rect.size.x) * image.rectTransform.rect.size.y;

		//Set new size
		image.rectTransform.sizeDelta = new Vector2(newWidth, newHeight);

		image.rectTransform.position = new Vector3(-newWidth/2, newHeight/2, image.rectTransform.position.z);
	}

}
