﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class BookmarkButton : MonoBehaviour, ISelectable {

	/// <summary>
	/// The sprite renderer.
	/// </summary>
	private SpriteRenderer spriteRenderer;

	/// <summary>
	/// The selected sprite.
	/// </summary>
	Sprite selectedSprite;

	/// <summary>
	/// The deselected sprite.
	/// </summary>
	Sprite deselectedSprite;

	/// <summary>
	/// The correct sprite.
	/// </summary>
	Sprite correctSprite;

	/// <summary>
	/// The observation.
	/// </summary>
	public ChapterObservation observation;

	/// <summary>
	/// The correct.
	/// </summary>
	private bool correct = false;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
		//Initialise the sprite renderer
		spriteRenderer = this.GetComponent<SpriteRenderer>();

		//Add the box collider at awake. This way it matches the size perfectly
		BoxCollider collider = this.gameObject.AddComponent<BoxCollider>();

		//Set the size of the box collider
		collider.size = spriteRenderer.bounds.size;
	}

	/// <summary>
	/// Raises the selected event.
	/// </summary>
	public void OnSelect()
	{
		//Invoke event
		GameControllerSingleton.sharedInstance.bookmarkButtonSelected(this);

		//Do nothing further if correct
		if (correct)
			return;
		
		//Set the sprite to selected
		spriteRenderer.sprite = selectedSprite;

		Debug.Log("bookmark pressed");
	}

	/// <summary>
	/// Raises the deselected event.
	/// </summary>
	public void OnDeselect()
	{
		//Nothing here. Is raised in the chapterController

		//Invoke event
		GameControllerSingleton.sharedInstance.bookmarkButtonDeselected(this);
	}

	/// <summary>
	/// Deselect this instance manual.
	/// </summary>
	public void SetDeselected()
	{
		if (correct)
			return;
		
		//Set the sprite to deselected
		spriteRenderer.sprite = deselectedSprite;
	}

	/// <summary>
	/// Set the selected state manual.
	/// </summary>
	public void SetSelected()
	{
		//Do nothing further if correct
		if (correct)
			return;

		//Set the sprite to selected
		spriteRenderer.sprite = selectedSprite;
	}

	/// <summary>
	/// Sets the sprite to the correct observation.
	/// </summary>
	public void SetCorrect()
	{
		spriteRenderer.sprite = correctSprite;
		correct = true;
	}

	/// <summary>
	/// Determines whether this instance is correct.
	/// </summary>
	/// <returns><c>true</c> if this instance is correct; otherwise, <c>false</c>.</returns>
	public bool IsCorrect()
	{
		//Return this field
		return correct;
	}

	/// <summary>
	/// Sets the sprites.
	/// </summary>
	/// <param name="selectedSprite">Selected sprite.</param>
	/// <param name="unselectedSprite">Unselected sprite.</param>
	/// <param name="correctSprite">Correct sprite.</param>
	public void SetSprites(Sprite selected, Sprite unselected, Sprite correct)
	{
		//Set all the sprite references
		selectedSprite = selected;
		deselectedSprite = unselected;
		correctSprite = correct;

		//Set the sprite
		spriteRenderer.sprite = selectedSprite;
	}
}
