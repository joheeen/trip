﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Group color.
/// </summary>
[System.Serializable]
public struct GroupColor {

	/// <summary>
	/// The color enum.
	/// </summary>
	public GroupColorEnum colorEnum;

	/// <summary>
	/// The color.
	/// </summary>
	public Color color;

}
